#include "i_vect.h"
#include "trap.h"


/*initializes interrupt vector at bootstrap*/
void init_i_vect() {
  INTERRUPT_VECTOR[ TRAP_KERNEL       ] = _trap_kernel;
  INTERRUPT_VECTOR[ TRAP_CLOCK        ] = _trap_clock;
  INTERRUPT_VECTOR[ TRAP_ILLEGAL      ] = _trap_illegal;
  INTERRUPT_VECTOR[ TRAP_MEMORY       ] = _trap_memory;
  INTERRUPT_VECTOR[ TRAP_MATH         ] = _trap_math;
  INTERRUPT_VECTOR[ TRAP_TTY_RECEIVE  ] = _trap_tty_receive;
  INTERRUPT_VECTOR[ TRAP_TTY_TRANSMIT ] = _trap_tty_transmit;
  INTERRUPT_VECTOR[ TRAP_DISK         ] = _trap_disk;


  //fill unused trap slots with an error function
  int i;
  for (i = TRAP_DISK + 1; i < TRAP_VECTOR_SIZE; i++)
    INTERRUPT_VECTOR[ i ] = _trap_error;
}
