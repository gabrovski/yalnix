// header files in include

#include "hardware.h"
#include "i_vect.h" //global interrupt table
#include "../proc_mgmt/process.h" //pcb stuff

unsigned int KernelDataStart;
unsigned int KernelDataEnd;

pte r0_pt[MAX_PTE_LEN]; //region 0 page table entries
pte r1_pt[MAX_PTE_LEN]; //region 1 page table entries

// data structures/global data

frame_stack_t free_frames_stack;
// queues for tty receive/transmit

//flag for vmem availability
short VMEM_ENABLED = 0;

//flag for highest address before vmem
unsignet int max_addr;

typedef struct frame {
  int pfn;
  void * next;
  // data to fill up frame 
} frame_dll_t;

typedef struct pipe {
  int pipe_idp;
  //receive_data queue;
}

void SetKernelData(void * KernelDataStart, void * KernelDataEnd) {
  KernelDataStart = (unsigned int) KernelDataStart;
  KernelDataEnd = (unsigned int) KernelDataEnd;
}

int SetKernelBrk(void *addr) { 
  // if vmem disabled, set kernel data end to rounded up addr
  // otherwise...
  // check if address doesn't go into stack frame
  // if it does error
  // otherwise make all pte's valid up to the address
  // while taking pfns off our free frame stack
  // then make everything above addr up to the stack frame invalid	
  // set kernel_brk

  unsigned int t = (((unsigned int) addr) & PAGEMASK) << PAGESHIFT;
  if (VMEM_ENABLED) {
    if (t >= KERNEL_STACK) 
      return ERROR;
    
    //check arithmetic
    int i;
    for (i = 0; VMEM_0_BASE << i*PAGESIZE < t; i++) {
      r0_pte[i].valid = 1;

      //TODO allocate PMEM
      alloc_frame(&free_frame_stack);
    }

  }
  else {
    //keep track of max passed address;
    //needed for enabling vm
    if (t > max_addr) 
      max_addr = t; 
    
    //why round up?
    KernelDataEnd = (unsigned int) addr;
  }
  return t;
}

void KernelStart(char *cmd_args[], int pmem_size, UserContext *uctxt) {
  init_i_vect();
  // give reg_vector_base interrupt_handler_t (the address of our table)
  // use malloc and pmemsize rounded down to the number of frames in pmem_size to create free_frames double-linked-list
  long i;
  for(i = 0; i < KernelDataStart >> PAGESHIFT; i++) {
    r0_pt[i]->pfn = i
      r0_pt[i]->valid = 1
      r0_pt[i]->prot = PROT_READ | PROT_EXEC
      // remove frame i from free frames
      }
  for(i = KernelDataStart >> PAGESHIFT; i < KernelDataEnd >> PAGESHIFT; i++) {
    r0_pt[i]->pfn = i
      r0_pt[i]->valid = 1
      r0_pt[i]->prot = PROT_READ | PROT_WRITE
      // remove frame i from free frames
      }
  r1_pt[MAX_PTE_LEN-1] -> valid = 1;
  // dequeue free frame and set pfn of r1_pt[-1]

  // write REG_PTBR0 = (unsigned int) r0_pt
  // write REG_PTBR1 = (unsigned int) r1_pt
  // write REG_PTLR1 = MAX_PTE_LEN
  // write REG_PTLR1 = MAX_PTE_LEN
  // set vmem register to enable
	
  // create idle process
  // load initial process using cmd_args
  // contexts are saved in pcbs
  // save contexts to uctxt pointer
  // return and hope it runs
}

KernelContext *MyKCS(KernelContext *kc_in, void *ready_pcb_p, void *next_pcb_p) {
  // save current registers to ready_pcb_p
  // put ready_pcb_p onto the ready queue??
  // put next_pcb_ps registers into the registers
  // this assumes we set everything up b4hand, which makes sense to me
}

// kernel calls
/*create a new pcb with that is the same as the parent
  return child id in parent and 0 in child*/
int Fork() {
  //somehow this will have the current pcb 
  pcb_t *parent = find_pcb(GetId());

  //assume we have malloc
  pcb_t *child = malloc(sizeof(pcb_t));
  if (child == NULL) {
    return -1; //error
  }
   
  //copy pcbs
  if (copy_from_to(parent, child) < 0)
    return -1;

  //get next process id, no need to sync
  int child_id = g_process_count+1;

  //adding new id
  pcb_t->id = child_id;
  
  //===TODO
  //===switch to child!
  //===copy over frames into corresponding page entries in the switch!
  
  if (GetId() == child->id)
    return 0;
  else
    return child->id;
}


int Exec(char *filename, char **argvec){
  //See LoadProgram
}

void Exit(int status){
  // If Child
  // Use Parentid To Find Parent Pcb
  // Set Exit Status of child in parent's pcb
  // if parent is blocked
  // set wait_for_child to 1
  // move the parent to ready queue - do that in wait!

  pcb_t *parent = ready_pcb->parent;

  if (parent != NULL) { //child
    corpse_t *dead = malloc(sizeof(corpse_t));

    if (dead == NULL) {
      //handle error!
      return;
    }

    dead->id = ready_pcb->id;
    dead->status = status;

    //add to pool of dead children
    add_node(parent->dead_children, dead);

    remove_node(ready_pcb, ready_pcb->id); //deals with free too
    
    add_node(ready_pcb, pop_node(blocked_pcb, parent->id))l
  }
  else {
    remove_node(ready_pcb, ready_pcb->id); //deals with free too
  }
}

int Brk(void * addr) {
  // check if address is below heap or in stack
  // if so return ERROR
  //set ptes to valid and free frames
  //set brk
}

int Wait(int * status_ptr) {
  // look at child list for child
  // if empty 
  // return ERROR
  // if there are no child processes that have finihsed
  // stick the pcb on the blocked queue
  // set waitforchild to 1
  // dequeue a child process and return its exit status to status id
  // return child id
  if (ready_pcb->children == NULL)
    return ERROR;

  while(ready_pcb->dead_children == NULL) 
    add_node(blocked_pcb, pop_node(ready_pcb, ready_pcb->id));
  
  corpse_t* dead = ready_pcb->dead_children;
  *status_ptr = dead->status;
  int id = dead->id;
  remove_node(ready_pcb->dead_children, id);
  return id;
}

int GetId() {
  if (ready_pcb != NULL)
    return ready_pcb->id;
  else
    return ERROR; //current process is NULL?!?!?!
}

int Delay(int clock_ticks) {
  // if clock ticks < 0 
  //return error
  // if clockticks == 0
  // return 0
  // stick curr pcb onto blocked queue
  // set pcb element clock ticks to clock_ticks
  // grab a process from ready 
  // return 0
}

int TtyRead(int tty_id, void *buf, int len) {
  // get tty_line from tty receive queue (don't pop yet)
  // if len > len(tty_line)
  // copy into the buffer, ignoring null chars
  // now pop
  // if tty_line is shorter than len
  // copy it into the buffer
  // now pop
  // if tty_line is empty
  // stick pcb onto the blocked queue and set wait_receive to 1
}

int TtyWrite(int tty_id, void * bbuf, int len) { 
  // call TTY transmit on buffer
  // note: segment the buffer and send it piecewise if it's too big
  // return len
  // set blocking flag of calling process pcb to 1 until we get an interrupt from tty transmit
}

int PipeInit(int * pipe_idp) { 
  // if error return error
  void * pipe = malloc(sizeof(struct pipe));
  if (pipe == NULL)
    return ERROR;

  pipe->id = SYNC_LL.next_id; //get next avail id
  add_sync_node(&SYNC_LL, pipe); //add to global queue of cvars, pipes nad locks
  
  return 1;
}

int PipeRead(int pipe_id, void *buf, int len) {
  //use pipe id to find corresponding pipe in queue
  //read from pipe data.  implementation of pipe read data queue will be like tty transmit
  // block by setting reading_from_pipe to 1 flag in pcb
  // return in case of error
}

int PipeWrite(int pipe_id, void *buf, int len) {
  // use pipe id to find pipe in queue
  // if can't find, return error
  // write into the pipe receive data like tty transmit
  // like in tty transmit set writing_to_pipe flag to 1
  // return when complete
}
