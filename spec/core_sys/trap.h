
#ifndef _trap_h
#define _trap_h

#include "../include/hardware.h"

/*prototypes for the trap functions*/

void _trap_kernel(UserContext *);
void _trap_clock(UserContext *);
void _trap_illegal(UserContext *);
void _trap_memory(UserContext *);
void _trap_math(UserContext *);
void _trap_tty_receive(UserContext *);
void _trap_tty_transmit(UserContext *);
void _trap_disk(UserContext *);
void _trap_error(UserContext *);


#endif
