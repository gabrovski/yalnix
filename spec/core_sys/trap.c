
#include "trap.h"

/*trap hadnlers pointed to by itnerrupt vector*/

void _trap_kernel(UserContext *u_con) {
  //calls the function denoted by the struct user_context member code
}

void _trap_clock(UserContext *u_con) { 
  // goes into the blocked queue and decreases clockticks if it's greater than 0
  // otherwise it moves it to the next process queue
  // switch in next process if there is one
}

void _trap_illegal(UserContext *u_con) { 
  //free up the pcb corresponding to the user context 
  //==> cant free, a process might be waiting on it. make it defunct and put it in a zombie queue or something
  //clearing registers etc,
  //then just context switch to the next one
}

void _trap_memory(UserContext *u_con) { 
  //check if the requested addr is lower than the kernel stack limit + a page
  // NOTE user process cannot access kernel data
  
  //if so, exit
  //else set bits up to addr to valid and pop frames as needed from frame stack
  
  //TODO check logic again
  // if below stack but above heap, allocate more frames
  //since in trap memory addr was def under stack
 if (u_con->addr > USER_BRK && u_con->addr < USR_STACK) {
   //allocate more frames
   int frame_count = (u_con->addr - USR_STACK) / FRAMESIZE + 1;
   //TODO allocate frame_count new frames and contnue execution
 }
  else {
    //exception
  }
 
}

void _trap_math(UserContext *u_con) { 
  //exit and throw the error code corresponding to trap math
}

void _trap_tty_receive(UserContext *u_con) { 
  // malloc max line (we have malloc via brk)
  // copy max line buffer to tty receive linked list
  // wake up any processes (we'll have a flag in our pcb)
}

void _trap_tty_transmit(UserContext *u_con) { 
  // just add data to transmit queue
  // i assume tty control device constantly reads from this queue
}

void _trap_disk(UserContext *u_con) { 
  // i don't think we have to do this yet
}

void _trap_error(UserContext *u_con) { 
  // in case an illegal trap is called alert OS
}
