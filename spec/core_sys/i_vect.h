
#ifndef _i_vect_h
#define _i_vect_h

#include "../include/hardware.h"

/*interrupt vector table*/
int INTERRUPT_VECTOR[TRAP_VECTOR_SIZE];

/*initializer for the interrupt vector to be called at bootstrap*/
void init_i_vect();

#endif
