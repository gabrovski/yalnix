

#include "../../../include/yalnix.h"

//TODO
//global linked list struct SYNC_LL
//to contain all defined locks, cvars and pipes, and a global next_id int
//to keep track of the next id for the next new sync structure or pipe
//each element of SYNC_LL is a SYNC_NODE with a void ptr to its specific structure
//  and a next pointer (and a prev pointer if needed)
typedef struct _sync_node__ {
  lock_t locks[MAX_LOCKS];
  int size;
} sync_node_t;

//global SYNC_LL linked list of pipes, cvars and locks
sync_node_t SYNC_LL;


//TODO
//not defined yet but searches all nodes in SYNC_LL and returns the one with the matching id
//returns the data void pointer of the SYN_NODE
void* find_by_id(SYNC_NODE*, int id);

//TODO
//function to add a new lock, cvar or pipe to SYNC_LL.
void add_sync_node(SYNC_NODE*, void *sync_str);

//TODO
//function to remove a lock, cvar or pipefrom SYNC_LL.
//returns it if it finds it, otehrwise NULL
void* remove_sync_node(SYNC_NODE*, void *sync_str);

/*cvar struct*/
typedef struct _cvar_ {
  int id;
  pcb_t *queue; //queued up waiting processes
} cvar_t;

/*lock struct*/
typedef struct _lock_ {
  int id;
  short locked; //is it locked?
} lock_t;


