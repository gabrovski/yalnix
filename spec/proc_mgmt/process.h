/*
 * Header file for a process definition
 */

#ifndef process_h
#define process_h

#include "../../include/hardware.h"


pcb_t *ready_pcb; //pointer to the beginning of the circular list
pcb_t *blocked_pcb; //pointer to blocked circular list
corpse_t *death_row; //pointer to dead processes list

//will keep track of ready queue, blocked queue and other necessary queues (cvar queues)
typedef struct pcb {
  int id;
  UserContext *u_con;
  KernelContext *k_con;

  //TODO need bookkeeping for the frames used by the process for the page table
  int frame_count;
  void *frame_addr; //each frame contains pointer to next frame?

  //circular linked list for the round robin
  pcb_t *next;
  pcb_t *prev;

  /*keep processes in the robin even after they are dead*/
  short alive; //is the process dead or not?

  //TODO
  //bookkeeping for some of the kernel call

  pcb_t *parent; //pointer to parent calling process for exit status notifcations
  
  //TODO
  pcb_t *children;
  corpse_t* dead_children; //Linked list of ids of exited children
} pcb_t;


/*helper function to copy a pcb to another
  creates new user and kernel contexts*/
int copy_from_to(pcb_t *, pcb_t *);

/*add a pcb to the round robin after the selected head*/
void add_proc(pcb_t *, pcb_t*);

/*find a pcb by the specified id*/
pcb_t *find_proc(pcb_t *, int);

//TODO 
//remove pcb from specified queue
pcb_t *remove_proc(pcb_t *, int);

#endif
