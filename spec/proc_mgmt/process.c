
#include "process.h"

//copy a pcb to another
//skip pid and frame_addr
int copy_from_to(pcb_t *from, pcb_t *to) {
  to->frame_count = from->frame_count;

  to->u_con = malloc(sizeof(UserContext));
  if (to->u_con == NULL) 
    return -1;

  to->k_con = malloc(sizeof(KernelContext));
  if (to->k_con == NULL) 
    return -1;
  
  memcpy(to->u_con, from->u_con, sizeof(UserContext));
  memcpy(to->k_con, from->k_con, sizeof(KernelContext));
  
  return 1;
}

/*add a new process after the head*/
void add_proc(pcb_t *head, pcb_t *new) {
  new->prev = head;
  new->next = head->next;
  head->next = new;
}

/*find a pcb by the specified id*/
pcb_t *find_proc(pcb_t *head, int pid) {
  pcb_t *curr = head;
  while (curr != NULL) 
    if (curr->pid == pid)
      return curr;
    else
      curr = curr->next;

  return NULL; //not found
}
