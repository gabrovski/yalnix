
#include "proc_sync.h"

int LockInit(int *lockidp) {
  lock_t *new_lock = malloc(sizeof(lock_t));
  if (new_lock == NULL) {
    return ERROR;
  }
  else {
    //add to the &SYNC_LL
    lock_t->id= &SYNC_LL->next_id;
    *lockidp = next_id;
    add_sync_node(&SYNC_LL, (void*) new_lock);
    return 1;
  }
}

int Acquire(int lock_id) {
  lock_t lock = (lock_t*)find_by_id(&SYNC_LL, lock_id);
  if (lock == NULL) {
    //exception, lock not set
    return ERROR;
  }
  else {
    while (lock->locked) {
      //wait for realease
    }
    
    lock->locked = 1;
    return 1;
  }
}

int Release(int lock_id) {
  lock_t *lock = (lock_t*)find_by_id(&SYNC_LL, lock_id);
  if (lock == NULL || !lock->locked)
    return ERROR;
  else {
    lock->locked = 1;
    return 1;
  }
}

int CvarInit(int *cvar_idp) {
  int next_id = &SYNC_LL->next_id;
  cvar_t *cvar = malloc(sizeof(cvar_t));
  if (cvar == NULL)
    return ERROR;

  else {
    cvar->id = next_id;
    *cvar_idp = next_id;
    add_sync_node(&SYNC_LL, (void*) cvar);
    return 1;
  }
}

int CvarSignal(int cvar_id) {
  cvar_t *cvar = find_cvar(cvar_id);
  if (cvar == NULL)
    return ERROR;
  else {
    //TODO
    //get a process out of the cvar queue
    //stick it on the ready queue 
    add_proc(curr_pcb, get_cvar_proc(cvar);
  }
}

int CvarBroadcast(int cvar_id) {
  cvar_t *cvar = find_cvar(cvar_id);
  if (cvar == NULL)
    return ERROR;
  else {
    //TODO
    //get all processes out of the cvar queue
    //stick them on the ready queue 
    while (cvar->queue!=NULL)
      add_proc(curr_pcb, get_cvar_proc(cvar);

    return 1;   
  }
}


//TODO check logic!
int CvarWait(int cvar_id, int lock_id) {
  int rc;
  rc = Release(lock_id);
  if (rc == ERROR)
    return ERROR;

  else {
    cvar_t *cvar = find_cvar(cvar_id);
    if (cvar == NULL)
      return ERROR;

    else {
      while (!cvar->condition) {
	//wait
      }
      
      rc = Acquire(lock_id);
      if (rc == ERROR)
	return ERROR;

      return 1;
    }
  }
}

//reclaim a pipe, cvar or lock
int Reclaim(void *id) {
  void *ptr = remove_by_id(&SYNC_LL, *id);
  if (ptr != NULL)
    return free(ptr);
  else
    return ERROR;
}
