#include "queue.h"

void *q_pop_data(queue_t *q) {
  if (q == NULL)
    return NULL;
  
  queue_t *head = q;
  q = q->next;

  void *res =head->data;
  free(head);
  return res;
}

queue_t *q_pop_node(queue_t *q) {
  if (q == NULL)
    return NULL;
  
  queue_t *head = q;
  q = q->next;

  head->next = NULL;
  return head;
}

//free
int q_remove(queue_t* q, int id) {
  if (q == NULL) 
    return ERROR;
  
  queue_t *curr = q;
  while (curr != NULL) {
    if (curr->id == id) {
      curr->prev->next = curr->next;
      curr->next->prev = curr->prev;
      free(curr->data); //needs to be malloced
      free(curr);
      return 1;
    }
    curr = curr->next;
  }

  return ERROR;
}


//add right before queue pointer 
//id is the id for the new element
int q_add_data(queue_t* q, void *data, int id) {
  queue_t *new = malloc(sizeof(queue_t));
  if (new == NULL)
    return ERROR;
  
  new->data = data;
  new->id = id;
  
  if (q == NULL)
    q = new;
  else {
    new->next = q;
    new->prev = q->prev;
    q->prev = new;
  }
  return id;
}

//add a node before q pointer
int q_add_node(queue_t* q, queue_t* new) {
  if (q == NULL)
    q = new;
  else {
    new->next = q;
    new->prev = q->prev;
    q->prev = new;
  }
  return 1;
}

//add end of queue
int q_add_data_tail(queue_t *q, void* data, int id) {
  if (q == NULL)
    q_add_data(q, data, id);
  else {
    queue_t *curr = q;
    while (curr->next != NULL)
      curr = curr->next;

    queue_t *new = malloc(sizeof(queue_t));
    if (new == NULL)
      return ERROR;
  
    new->data = data;
    new->id = id;
    curr->next = new;
    new->prev = curr;
  }
  return 1;
}

//add end of queue
int q_add_node_tail(queue_t *q, queue_t *new) {
  if (q == NULL)
    q_add_node(q, new);
  else {
    queue_t *curr = q;
    while (curr->next != NULL)
      curr = curr->next;

    curr->next = new;
    new->prev = curr;
  }
  return 1;
}
