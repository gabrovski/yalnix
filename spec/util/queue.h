#ifndef queue_h
#define queue_h

#include <stdlib.h>
#include "../../include/hardware.h"
#include "../../include/yalnix.h"
typedef struct _queue {
  int id;
  struct _queue *next, *prev;
  void *data;
} queue_t;

void *q_pop_data(queue_t*);
queue_t *q_pop_node(queue_t*);

int q_remove(queue_t*, int);

int q_add_data(queue_t*, void*, int);
int q_add_node(queue_t*, queue_t*);

int q_add_data_tail(queue_t*, void*, int);
int q_add_node_tail(queue_t*, queue_t*);

#endif
