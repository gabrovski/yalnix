/*
  File: syscallTest.c
  Author: James Oakley and Katelin Bailey
  Project: Yalnix (Team TINN)
  Date: November 09
  Description: test of Yalnix syscalls
*/


#include <hardware.h>
#include <yalnix.h>
#include "util.h"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
//#include "extracalls.h"

#define SUCCESS 0

void spawnShell(int tty)
{
  int pid;
  if(0==(pid=Fork()))
  {
    TracePrintf(0,"init child running and has pid %i\n",GetPid());
    char* args[3];
    char buf[16];
    sprintf(buf,"%i",tty);
    args[0]="./ysh";
    args[1]=buf;
    args[2]=NULL;
    Exec("./ysh",args);
    char* msg="Failed to start shell!\n";
    TtyWrite(0,msg,strlen(msg));
    Exit(-1);
  }
}

int forkHello(int ttyno,int delay)
{
  int pid=Fork();
  yassert(pid>=0);
  if(0==pid)
  {
    //child process
    char buf1[32];
    char buf2[32];
    sprintf(buf1,"%i",ttyno);
    sprintf(buf2,"%i",delay);
    char* args[4];
    args[0]="./exec";
    args[1]=buf1;
    args[2]=buf2;
    args[3]=NULL;
    int ret=Exec(args[0],args);
    yassert(ERROR==ret);
    TracePrintf(0,"Failed to start hello program, which should work fine\n");
    Exit(-1);
  }
  return pid;
}

void forkWaitTest(int ttyno){
  //can't wait for children now, make sure get error back
  int status;
  TracePrintf(0,"syscallTest waiting when no children, this should fail\n");
  yassert(ERROR==Wait(&status));

  
  //first fork of a child which should exec properly
  int pid=forkHello(ttyno,1);

  TracePrintf(0,"syscallTest waiting for hello world child, this should succeed\n");
  yassert(pid==Wait(&status));
  yassert(0==status);

  TracePrintf(0, "syscallTest about to fork multiple forkHellos\n");
  //now fork multiple children and wait for both of them
  int pid1=forkHello(ttyno,7);
  int pid2=forkHello(ttyno,2);
  int pid3=forkHello(ttyno,3);
  TracePrintf(0,"Expect children to exit in the order %i,%i,%i\n",pid2,pid3,pid1);
  yassert(pid2==Wait(&status));
  yassert(0==status);
  yassert(pid3==Wait(&status));
  yassert(0==status);
  yassert(pid1==Wait(&status));
  yassert(0==status);


  //now fork off a child that won't exec properly
  pid=Fork();
  yassert(pid>=0);
  if(0==pid)
  {
    //child process
    char* args[2];
    args[0]="IDontExist";
    args[1]=NULL;
    int ret=Exec(args[0],args);
    yassert(ERROR==ret);
    Exit(42);
  }

  
  TracePrintf(0,"syscallTest ensuring that wait returns error on NULL status\n");
  //see if wait returns ERROR if we pass it a NULL pointer
  yassert(ERROR==Wait(NULL));

  TracePrintf(0,"syscallTest waiting for child which won't be able to execute\n");
  pid = Wait(&status);
  yassert(42==status);
  
  //no running children, make sure Wait returns arrer
  TracePrintf(0,"syscallTest ensuring that wait returns error on no running children\n");
  yassert(ERROR==Wait(&status));
  TracePrintf(0, "done with forkWaitTest\n");
}




void delayTest(){
  //test Delay
  TracePrintf(0,"Testing Delay\n");
  TracePrintf(10,"Delay 2\n");
  yassert(0==Delay(2));
  TracePrintf(10,"Delay 12\n");
  yassert(0==Delay(12));
  TracePrintf(10,"Delay 1\n");
  yassert(0==Delay(1));//just to make sure multiple Delays are ok
  TracePrintf(10,"Delay 0\n");
  yassert(0==Delay(0));//should be immediate
  TracePrintf(10,"Delay -1\n");
  yassert(ERROR==Delay(-1));

  //test multiple delays
  TracePrintf(10, "Forked delays\n");
  int pid = Fork();
  yassert(pid!=ERROR);
  int status;
  if(pid == 0){
    TracePrintf(10,"Delay 12\n");
    yassert(0== Delay(12));
    TracePrintf(10,"Out of 12\n");
    pid = Fork();
    yassert(ERROR!=pid);
    if(pid == 0){
      TracePrintf(10,"Delay 5\n"); 
      yassert(0== Delay(5));
      TracePrintf(10,"Out of 5\n");
      Exit(0);
      yassert(0);//shouldn't be here
    }
    TracePrintf(10,"Delay 4\n"); 
    yassert(0== Delay(4));
    TracePrintf(10,"Out of 4\n");
    yassert(ERROR!=Wait(&status));
    Exit(0);
    yassert(0);//shouldn't be here
  }
  else{
    TracePrintf(10,"Delay 10\n"); 
    yassert(0== Delay(10));
    TracePrintf(10,"Out of 10\n");
    yassert(ERROR!=Wait(&status));
  }
}

void pipeTest(){
  TracePrintf(0,"Starting pipe test\n");
  int ret[3];
  yassert(ERROR!=PipeInit(&ret[0]));
  yassert(ERROR!=PipeInit(&ret[1]));
  yassert(ERROR!=PipeInit(&ret[2]));
  yassert(ERROR ==PipeInit(NULL));

  char *msg1 = "piglets run fast: ";
  char *msg2 = "they make me jealous, you know";
  char *msg3 = "that's pretty cool";
  int len  = strlen(msg1)+strlen(msg2)+strlen(msg3);
  char *buf = malloc(len+1);
  yassert(buf);//we're assuming we won't run out of memory
  //if this yassert fails it isn't a problem with the OS per se,
  //(since we could run out of pmem)
  //but it means our test is invalid

  //delays are not necessary,
  //they could be commented out. They affect if pipe read will block or not
  int rc = Fork();
  if(rc == 0){
    Delay(12);
    TracePrintf(0, "Pipe read on %d\n", ret[0]);
    yassert(SUCCESS ==PipeRead(ret[0], buf, strlen(msg1)+20));
    buf[strlen(msg1)+21] = '\0';
    TracePrintf(0, "recieved: %s\n", buf);
    Delay(7);
    yassert(SUCCESS ==PipeRead(ret[0], buf, strlen(msg3)+strlen(msg2)-20));
    buf[strlen(msg3)+strlen(msg2)-20] = '\0';
    TracePrintf(0, "recieved: %s,\n", buf);
    Exit(0);
  }
  else{
    Delay(rc);
    TracePrintf(0, "Write %s\n", msg1);
    yassert(SUCCESS ==PipeWrite(ret[0], msg1, strlen(msg1)));
    yassert(SUCCESS ==Delay(3));
    TracePrintf(0, "Write %s\n", msg2);
    yassert(SUCCESS ==PipeWrite(ret[0], msg2, strlen(msg2)));
    TracePrintf(0, "Write %s\n", msg3);
    yassert(SUCCESS ==PipeWrite(ret[0], msg3, strlen(msg3)));
    TracePrintf(0, "Writes succeeded\n");
    int status;
    Wait(&status);
    yassert(SUCCESS == status);
  }
  TracePrintf(0, "Checking invalid pipe actions\n");
  //Now invalids
  yassert(ERROR ==PipeWrite(ret[0]-5, msg1, 6));
  yassert(ERROR ==PipeWrite(ret[0], NULL, 6));
  yassert(ERROR ==PipeWrite(ret[0], msg1, -1));
  TracePrintf(0, "Checked first 3 invalid pipe actions\n");

  yassert(ERROR ==PipeRead(ret[0]-5, msg1, 6));
  yassert(ERROR ==PipeRead(ret[0], NULL, 6));
  yassert(ERROR ==PipeRead(ret[0], msg1, -1));
  TracePrintf(0, "Checked last 3 invalid pipe actions\n");


  //Now reclaims
  yassert(SUCCESS == Reclaim(ret[0]));
  yassert(SUCCESS == Reclaim(ret[1]));
  yassert(SUCCESS == Reclaim(ret[2]));
  yassert(ERROR == Reclaim(ret[0]));
  yassert(ERROR == Reclaim(-2));
  TracePrintf(0,"Done pipe test\n");
}


void semaphoreTest(){
  TracePrintf(0,"Initializing sems\n");
  int ret[3];
  SemInit(&ret[0], 0);
  SemInit(&ret[1], 1);
  SemInit(&ret[2], 6);
  yassert(ERROR ==SemInit(NULL, 7));

  TracePrintf(0,"Testing sems\n");
  int pid1 = Fork();
  if(pid1 == 0){
    yassert(SUCCESS ==SemDown(ret[2]));
    yassert(SUCCESS ==SemDown(ret[1]));
    TracePrintf(0,"delaying sems for 7\n");
    Delay(7);

    TracePrintf(0,"upping sems\n");
    yassert(SUCCESS == SemUp(ret[1]));
    yassert(SUCCESS == SemUp(ret[0]));
    Exit(0);
  }
  Delay(3);

  TracePrintf(0,"delaying sems for 3\n");
  yassert(SUCCESS == SemDown(ret[0]));

  TracePrintf(0,"down sems null\n");
  yassert(ERROR == SemDown(ret[3]));

  yassert(SUCCESS == SemUp(ret[1]));
  yassert(SUCCESS == SemUp(ret[1]));
  yassert(SUCCESS == SemUp(ret[1]));

  TracePrintf(0,"down sems last\n");
  yassert(SUCCESS == SemDown(ret[2]));
  yassert(SUCCESS == SemDown(ret[2]));
    
  TracePrintf(0,"reclaim sems \n");
  yassert(SUCCESS == Reclaim(ret[0]));
  yassert(ERROR == SemDown(ret[0]));
  yassert(ERROR == SemUp(ret[0]));
  
  TracePrintf(0,"about to get status\n");

  int status;
  Wait(&status);
  yassert(SUCCESS ==status);

  
  TracePrintf(0,"Testing sems finished\n");
  return;
}


void cvarLockTest(){
  TracePrintf(0,"Initializing locks\n");
  int ret[6];
  CvarInit(&ret[0]);
  CvarInit(&ret[1]);
  CvarInit(&ret[2]);
  TracePrintf(0, "Initialized 3 cvar locks\n");
  yassert(ERROR ==CvarInit(NULL));
  LockInit(&ret[3]);
  LockInit(&ret[4]);
  LockInit(&ret[5]);
  TracePrintf(0, "Initialized 3 locks\n");
  yassert(ERROR ==LockInit(NULL));

  yassert(ERROR==Release(ret[1]));
  yassert(ERROR==Release(-1));
  yassert(ERROR==Release(999));
  
  TracePrintf(0,"Testing locks\n");
  //Just locks
  int pid1 = Fork();
  if(pid1 == 0){
    yassert(SUCCESS ==Acquire(ret[3]));
    yassert(SUCCESS ==Acquire(ret[4]));
    Delay(7);
    yassert(SUCCESS ==Release(ret[3]));
    yassert(ERROR ==Release(ret[3]));
    yassert(SUCCESS == Release(ret[4]));
    Exit(0);
  }
  else{
    int pid2 = Fork();
    if(pid2 == 0){
      TracePrintf(0, "Starting child\n");
      yassert(SUCCESS == Acquire(ret[3]));
      yassert(ERROR == Acquire(ret[3]));
      yassert(SUCCESS == Release(ret[3]));
      TracePrintf(0, "successfully acquired released and lock 3\n");
      yassert(SUCCESS == Acquire(ret[5]));
      yassert(SUCCESS == Reclaim(ret[5]));
      yassert(ERROR == Release(ret[5]));
      yassert(ERROR == Acquire(ret[5]));
      TracePrintf(0, "successfully acquired and released lock 5\n");
      Exit(0);
    }
    yassert(SUCCESS == Acquire(ret[3]));
    yassert(SUCCESS == Release(ret[3]));
    yassert(ERROR == Acquire(-1));
    yassert(ERROR == Release(ret[4]));
    
    int status;
    TracePrintf(0, "about to wait\n");
    yassert(ERROR!=Wait(&status));
    TracePrintf(0, "return from wait\n");
    yassert(SUCCESS ==status);
    TracePrintf(0, "about to wait again\n");
    yassert(ERROR!=Wait(&status));
    TracePrintf(0, "return from wait again\n");
    yassert(SUCCESS ==status);
  }

  TracePrintf(0,"Testing cvars\n");
  //Cvars and locks
  pid1 = Fork();
  yassert(ERROR!=pid1);
  if(pid1 == 0){
    //invalids
    yassert(ERROR ==CvarWait(-1, ret[3]));
    yassert(ERROR ==CvarWait(ret[3], ret[3]));
    yassert(ERROR ==CvarWait(ret[3], -1));

    //reals
    yassert(SUCCESS ==Acquire(ret[3]));
    TracePrintf(2,"valid cvarwait1\n");
    yassert(SUCCESS ==CvarWait(ret[0], ret[3]));
    yassert(SUCCESS==Release(ret[3]));
    yassert(SUCCESS ==Acquire(ret[4]));
    TracePrintf(2,"valid cvarwait2\n");
    yassert(SUCCESS == CvarWait(ret[1], ret[4]));
    yassert(SUCCESS == Release(ret[4]));
    TracePrintf(0,"Leaving 1st cvar test child\n");
    Exit(0);
  }
  else{
    int pid2 = Fork();
    if(pid2 == 0){
      yassert(ERROR ==CvarWait(ret[0], ret[5]));
      yassert(SUCCESS ==Acquire(ret[4]));
      TracePrintf(2,"valid cvarwait3\n");
      yassert(SUCCESS ==CvarWait(ret[0], ret[4]));
      yassert(SUCCESS ==Release(ret[4]));
      yassert(SUCCESS ==Acquire(ret[3]));
      TracePrintf(2,"valid cvarwait4\n");
      yassert(SUCCESS ==CvarWait(ret[1], ret[3]));
      yassert(SUCCESS ==Release(ret[3]));
      TracePrintf(0,"Leaving 2nd cvar test child\n");
      Exit(0);
    }
    TracePrintf(1,"main proc which will issue broadcast running\n");
    Delay(12);
    TracePrintf(1,"main proc which will issue broadcast past 1st delay\n");
    yassert(SUCCESS == CvarSignal(ret[0]));
    Delay(7);
    TracePrintf(1,"main proc which will issue broadcast past 2nd delay\n");
    yassert(SUCCESS == CvarSignal(ret[0]));
    Delay(5);
    TracePrintf(1,"main proc which will issue broadcast past 3rd delay\n");
    yassert(SUCCESS == CvarBroadcast(ret[1]));
    yassert(SUCCESS == CvarBroadcast(ret[2]));
    yassert(ERROR == CvarSignal(-1));
    yassert(ERROR == CvarBroadcast(-1));

    TracePrintf(0,"Waiting on status\n");
    int status;
    Wait(&status);
    yassert(SUCCESS ==status);
    Wait(&status);
    yassert(SUCCESS ==status);
  }
    


  TracePrintf(0,"Testing reclaim\n");
  //reclaims
  yassert(SUCCESS == Reclaim(ret[0]));
  yassert(SUCCESS == Reclaim(ret[1]));
  yassert(SUCCESS == Reclaim(ret[2]));
  yassert(SUCCESS == Reclaim(ret[3]));
  yassert(SUCCESS == Reclaim(ret[4]));
  yassert(ERROR == Reclaim(ret[0]));
  yassert(ERROR == Reclaim(ret[3]));
  yassert(ERROR == Reclaim(-2));
}



void brkTest(){
  TracePrintf(0,"Testing Brk\n");
  //test Brk
  //normal something probably between stack and heap
  TracePrintf(0,"Increasing Brk\n");
  yassert(0==Brk((void*)VMEM_1_LIMIT-5*PAGESIZE));
  //do the same thing again to make sure the OS is ok with that
  TracePrintf(0,"Increasing Brk again\n");
  yassert(0==Brk((void*)VMEM_1_LIMIT-5*PAGESIZE));
  //decrease the Brk
  TracePrintf(0,"Decreasing Brk\n");
  yassert(0==Brk((void*)VMEM_1_LIMIT-7*PAGESIZE));

  //put the Brk in the stack
  yassert(ERROR==Brk((void*)VMEM_1_LIMIT-PAGESIZE/2));
  //would be nice to test the red zone, but hard to know exactly where it is

  //test if Brk out of bounds
  yassert(ERROR==Brk((void*)VMEM_1_LIMIT+1));
  yassert(ERROR==Brk((void*)-1));
  yassert(0==Brk((void*)VMEM_1_LIMIT - 100*PAGESIZE));

  TracePrintf(0,"Done testing Brk\n");
}


void ttyWriteTest(int ttyno){
  TracePrintf(0,"starting tty write test\n");
  char *msg;
  //test TtyWrite
  msg="foo";
  yassert(ERROR==TtyWrite(42,msg,strlen(msg)));
  yassert(ERROR==TtyWrite(ttyno,msg,-1));
  yassert(ERROR==TtyWrite(ttyno,NULL,strlen(msg)));
  msg="you should see the letter e at the end";
  TtyWrite(ttyno,msg,strlen(msg));
  char longbuf[TERMINAL_MAX_LINE*3];
  for(int i=0;i<3*TERMINAL_MAX_LINE;i++)
  {
    if(i<TERMINAL_MAX_LINE/2)
    {
      longbuf[i]='a';
    }
    else if(i<TERMINAL_MAX_LINE)
    {
      longbuf[i]='b';
    }
    else if(i<2*TERMINAL_MAX_LINE)
    {
      longbuf[i]='c';
    }
    else
    {
      longbuf[i]='d';
    }
  }
  longbuf[TERMINAL_MAX_LINE*3-3]='e';
  longbuf[TERMINAL_MAX_LINE*3-2]='\n';
  longbuf[TERMINAL_MAX_LINE*3-1]='\0';

  TtyWrite(ttyno,longbuf,strlen(longbuf));
  TracePrintf(0,"done tty write test\n");
}


void ttyReadTest(int ttyno){
  char* msg;
   //now test TtyRead
  char buf1[2];
  char buf2[3];
  yassert(ERROR==TtyRead(42,buf1,2));
  yassert(ERROR==TtyRead(ttyno,buf1,-1));
  yassert(ERROR==TtyRead(ttyno,NULL,2));
  TracePrintf(0,"now testing tty reads that should work\n");

  msg="please enter the character 'a'\n";
  TtyWrite(ttyno,msg,strlen(msg));
  yassert(2==TtyRead(ttyno,buf1,2));
  yassert(!strncmp(buf1,"a\n",2));
  msg="please enter the string 'bc'\n";
  TtyWrite(ttyno,msg,strlen(msg));
  yassert(2==TtyRead(ttyno,buf1,2));
  TracePrintf(0, "buf1: %s\n", buf1);
  yassert(!strncmp(buf1,"bc",2));
  //the newline from bc won't be read, so we'll get it in the next read
  yassert(1==TtyRead(ttyno,buf2,3));
  yassert(!strncmp(buf2,"\n",1));
  msg="please enter the string 'bc' again\n";
  TtyWrite(ttyno,msg,strlen(msg));
  yassert(3==TtyRead(ttyno,buf2,3));
  yassert(!strncmp(buf2,"bc\n",3));
}



int main(int argc,char** argv)
{
  int ttyno=1;
  if(argc > 1)
  {
    ttyno=atoi(argv[1]);
  }
  
  char* msg="syscallTest running\n";
  TracePrintf(0, msg);
  TtyWrite(ttyno,msg,strlen(msg));
  TracePrintf(0, "%s", msg);

  semaphoreTest();
  
  brkTest();
  forkWaitTest(ttyno);
  delayTest();
  cvarLockTest();
  brkTest();
  pipeTest();
  //semaphoreTest();
  TracePrintf(0,"Doing cvarlocktest for the second time\n");
  cvarLockTest();
  ttyWriteTest(ttyno);
  ttyReadTest(ttyno);
  semaphoreTest();
  
  msg="syscallTest exiting. If this was run as the init process, the system will now halt. If you have gotten this far the test has SUCCEEDED and the system halt following this message does not indicate failure\n";
  TracePrintf(0,msg);
  TtyWrite(ttyno,msg,strlen(msg));
}
