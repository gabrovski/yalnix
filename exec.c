#include "include/hardware.h"
#include <stdlib.h>
#include "include/yalnix.h"

int main(int argc, char* argv[]) {
  int ttyno = atoi(argv[1]);
  int delay = atoi(argv[2]);
  TracePrintf(1, "exec.c: starting.  printing hello to tty: %d\n", ttyno);
  TtyPrintf(ttyno, "hello\n");
  Delay(delay);
  TracePrintf(1, "exec.c: init.c: successfully executed instruction. finishing\n");
}
