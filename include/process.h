#ifndef process_h
#define process_h

#include "../include/hardware.h"

typedef struct pcb {
  int pid;
  UserContext u_con;
  KernelContext k_con;
  
  struct pte *reg_ptbr0; 
  void *reg_ptlr0; 
  struct pte *reg_ptbr1; 
  void *reg_ptlr1;

  int brk_index;

  u_long kernel_stack_pfns[2]; // 0 is the base, 1 is the top of the stack

  //TODO need bookkeeping for the frames used by the process for the page table
  //int frame_count;
  //void *frame_addr; //each frame contains pointer to next frame?

  /*keep processes in the robin even after they are dead*/
  //short alive; //is the process dead or not?

  //TODO
  //bookkeeping for some of the kernel call

  struct pcb *parent; //pointer to parent calling process for exit status notifcations

  int block_ticks;
  int wait_for_child;
  //TODO
  Queue *children;
  Queue *child_statuses;
  //sync ids
  Queue *sync_queue;
  int sync_id;
  //corpse_t* dead_children; //Linked list of ids of exited children
} pcb_t;

typedef struct e_stat_ {
  int exit_status;
  int pid;
} e_stat_t;


int g_process_count;

#endif
