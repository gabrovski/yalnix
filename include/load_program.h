#ifndef load_program_h
#define load_program_h

#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "../include/hardware.h"
#include "../include/yalnix.h"
#include "../include/trap.h"
#include "../include/load_info.h"
#include "process.h"
#include "../algos-and-data-structures/queue.h"


//==>> #include anything you need for your kernel here

#define KILL -9
#define SUCCESS 1

extern struct pte r1_pt[MAX_PT_LEN];

int LoadProgram(char *name, char *args[], pcb_t *proc);

#endif
