#ifndef sys_calls_h
#define sys_calls_h

#include "hardware.h"
#include "trap.h"
#include "yalnix.h"
#include "../algos-and-data-structures/queue.h"
#include "load_program.h"
#include "process.h"

#include <stdlib.h>

//define types of syncrhonization structs
#define PIPE 'p'
#define CVAR 'c'
#define LOCK 'l'
#define SEMP 's'

extern Queue* dead_queue;
extern Queue* free_frames_queue;
extern Queue* ready_queue;
extern Queue* blocked_queue;

extern pcb_t* g_curr_pcb_p;
extern pcb_t init_pcb;

// book keeping for tty write/transmit
extern Queue* waiting_to_write_queues[NUM_TERMINALS];
// ^ queue of processes waiting to run TtyWrite
extern int data_segments_to_transmit[NUM_TERMINALS];
// ^ # of segments of currently writing process to transmit
// 0 when done
extern pcb_t* writing_pcb_p[NUM_TERMINALS];  
// ^ currently writing process
// NULL if no currenlty writing process

extern Queue* data_segments_to_receive[NUM_TERMINALS];
extern Queue* waiting_to_read[NUM_TERMINALS];

extern Queue *sync_queue; //queue to hold synchronization structs
extern g_sync_id;

void* sync_queue_find(Queue *sq, int id);

typedef struct pipe_data_ {
  int size;
  int lseek;
  void *buf;
} pipe_data_t;

typedef struct pipe_ {
  Queue *data; //FIFO queue of data segments into the pipe
  //Queue of processes waiting to read
  Queue *waiting_to_read;
} pipe_t;

typedef struct lock_ {
  int held_by;
  short locked;
} lock_t;

typedef struct cvar_ {
  Queue* procs; //queue of processes waiting on the cvar
} cvar_t;

typedef struct semp_ {
  int capacity;
  int basket;
  Queue* procs; //queue of processes waiting on the semp
} semp_t;

typedef struct sync_str_ {
  int id;
  char type;
  void* str; //pipe, cvar or lock
} sync_str_t;

#endif
