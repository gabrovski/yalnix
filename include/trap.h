#ifndef _trap_h
#define _trap_h

#include "../include/hardware.h"
#include "../algos-and-data-structures/queue.h"

/*prototypes for the trap functions*/

extern void* user_brk;
extern Queue* free_frames_queue;
extern Queue* ready_queue;
extern Queue* blocked_queue;
extern void* INTERRUPT_VECTOR[TRAP_VECTOR_SIZE];
extern KernelContext *MyKCS(KernelContext *kc_in, void *curr_pcb_pter, void *next_pcb_pter);
extern KernelContext *SetKernelContext(KernelContext *kc_in, void *curr_pcb_pter, void *next_pcb_pter);
extern KernelContext *CpKernelContext(KernelContext *kc_in, void *curr_pcb_pter, void *next_pcb_pter);
extern int g_clock_ticks;
extern UserContext *g_u_con;

void _trap_kernel(UserContext *);
void _trap_clock(UserContext *);
void _trap_illegal(UserContext *);
void _trap_memory(UserContext *);
void _trap_math(UserContext *);
void _trap_tty_receive(UserContext *);
void _trap_tty_transmit(UserContext *);
void _trap_disk(UserContext *);
void _trap_error(UserContext *);

void init_i_vect();


#endif
