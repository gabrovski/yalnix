/*
  File: util.h (userland version)
  Project: Yalnix (Team TINN)
  Author: James Oakley
  Date: Oct, 2009
  Description: misc. utility functions (userland version)
*/

#define MALLOC_CHECK(a)  if ((a) == NULL)   {                     \
    TracePrintf(0,"Malloc failed at %s:line %d ", __FILE__, __LINE__); \
    Exit(-1);                                                          \
  }


//asert a condition, program dies if it's not true
#define yassert(condition)  if(!(condition)){TracePrintf(0,"Userland yassert " #condition " failed at %s:line %i\n",__FILE__,__LINE__);Exit(-1);}


