#include "../include/trap.h"
#include "../include/process.h"
#include "../include/hardware.h"
#include "../include/yalnix.h"
#include <unistd.h>

extern pcb_t* g_curr_pcb_p;
extern pcb_t idle_pcb;

extern Queue* waiting_to_write_queues[NUM_TERMINALS];
extern int data_segments_to_transmit[NUM_TERMINALS];
extern pcb_t* writing_pcb_p[NUM_TERMINALS]; 

extern Queue* data_segments_to_receive[NUM_TERMINALS];
extern Queue* waiting_to_read[NUM_TERMINALS];

/*trap hadnlers pointed to by itnerrupt vector*/
void _trap_kernel(UserContext *u_con) {
  //calls the function denoted by the struct user_context member code
  pcb_t *curr = g_curr_pcb_p;
  memcpy(&(curr->u_con), u_con, sizeof(UserContext));

  int i;
  g_u_con = u_con;
  TracePrintf(1,"_trap_kernel: trap u_con %x %d\n", u_con, queue_size(free_frames_queue));

  switch (u_con->code) {
  case  YALNIX_BRK:
    TracePrintf(1,"_trap_kernel: brk call with addr %x %x\n", u_con->addr, u_con);
    u_con->regs[0] = (u_long) Brk(u_con->regs[0]);
    break;
    
  case YALNIX_DELAY:
    TracePrintf(1,"_trap_kernel: Delay call with addr %x %x\n", u_con->regs[0], u_con);
    u_con->regs[0] = (u_long) Delay(u_con->regs[0]);
    break;
    
  case YALNIX_GETPID:
    u_con->regs[0] = (u_long) GetPid();
    break;
    
  case YALNIX_FORK:
    TracePrintf(1,"_trap_kernel: Fork call\n!");
    u_con->regs[0] = (u_long) Fork();
    break;
  
  case YALNIX_EXEC:
    TracePrintf(1,"_trap_kernel: Exec called\n!");
    u_con->regs[0] = (u_long) Exec((char*) (u_con->regs[0]), (char**) (u_con->regs[1]));
    break;
    
  case YALNIX_EXIT:
    TracePrintf(1,"_trap_kernel: Exit called\n");
    Exit((int) u_con->regs[0]);
    break;
  
  case YALNIX_WAIT:
    TracePrintf(1,"_trap_kernel: Wait called\n");
    u_con->regs[0] = Wait(u_con->regs[0]);
    break;
  
  case YALNIX_TTY_WRITE:
    TracePrintf(1,"_trap_kernel: TtyWrite Called\n");
    u_con->regs[0] = TtyWrite(u_con->regs[0], u_con->regs[1], u_con->regs[2]);
    break;

  case YALNIX_TTY_READ:
    TracePrintf(1, "_trap_kernel: TtyRead Called\n");
    u_con->regs[0] = TtyRead(u_con->regs[0], u_con->regs[1], u_con->regs[2]);
    break;

  case YALNIX_PIPE_INIT:
    TracePrintf(1, "_trap_kernel: PipeInit Called\n");
    u_con->regs[0] = PipeInit(u_con->regs[0]);
    break;

  case YALNIX_PIPE_WRITE:
   TracePrintf(1, "_trap_kernel: PipeWrite Called\n");
   u_con->regs[0] = PipeWrite(u_con->regs[0], u_con->regs[1], u_con->regs[2]);
   break;

 case YALNIX_PIPE_READ:
   TracePrintf(1, "_trap_kernel: PipeRead Called\n");
   u_con->regs[0] = PipeRead(u_con->regs[0], u_con->regs[1], u_con->regs[2]);
   break;

 case YALNIX_LOCK_INIT:
   TracePrintf(1, "_trap_kernel: LockInit Called\n");
   u_con->regs[0] = LockInit(u_con->regs[0]);
   break;

 case YALNIX_LOCK_ACQUIRE:
   TracePrintf(1, "_trap_kernel: Acquire Called\n");
   u_con->regs[0] = Acquire(u_con->regs[0]);
   break;

 case YALNIX_LOCK_RELEASE:
   TracePrintf(1, "_trap_kernel: Release Called\n");
   u_con->regs[0] = Release(u_con->regs[0]);
   break;

 case YALNIX_CVAR_INIT:
   TracePrintf(1, "_trap_kernel: CvarInit Called\n");
   u_con->regs[0] = CvarInit(u_con->regs[0]);
   break;

 case YALNIX_CVAR_SIGNAL:
   TracePrintf(1, "_trap_kernel: CvarSignal Called\n");
   u_con->regs[0] = CvarSignal(u_con->regs[0]);
   break;

 case YALNIX_CVAR_BROADCAST:
   TracePrintf(1, "_trap_kernel: CvarBroadcast Called\n");
   u_con->regs[0] = CvarBroadcast(u_con->regs[0]);
   break;

 case YALNIX_CVAR_WAIT:
   TracePrintf(1, "_trap_kernel: CvarWait Called\n");
   u_con->regs[0] = CvarWait(u_con->regs[0], u_con->regs[1]);
   break;

 case YALNIX_RECLAIM:
   TracePrintf(1, "_trap_kernel: Reclaim Called\n");
   u_con->regs[0] = Reclaim(u_con->regs[0]);
   break;

 case YALNIX_SEM_INIT:
   TracePrintf(1, "_trap_kernel: SemInit Called\n");
   u_con->regs[0] = SemInit(u_con->regs[0], u_con->regs[1]);
   break;

 case YALNIX_SEM_UP:
   TracePrintf(1, "_trap_kernel: SemUp Called\n");
   u_con->regs[0] = SemUp(u_con->regs[0]);
   break;

 case YALNIX_SEM_DOWN:
   TracePrintf(1, "_trap_kernel: SemDown Called\n");
   u_con->regs[0] = SemDown(u_con->regs[0]);
   break;

  }
}

void _trap_clock(UserContext *u_con) { 
  TracePrintf(1, "trapclock: pc %x sp %x\n", u_con->pc, u_con->sp);
  // goes into the blocked queue and decreases clockticks if it's greater than 0
  // otherwise it moves it to the next process queue
  // switch in next process if there is one
  QueueEntry *entry = blocked_queue->head;
  int i = 0;
  int qsize = queue_size(blocked_queue);
  for(i = 0; i < qsize; i++) {
    pcb_t *blocked = (pcb_t*) queue_pop_head(blocked_queue);
    blocked->block_ticks--;
    if ((blocked->block_ticks < 1) && (blocked->wait_for_child) == 0) {
      queue_push_tail(ready_queue, blocked);
    }
    else {
      queue_push_tail(blocked_queue, blocked);
    }
  }

  //TracePrintf(1, "queue trap01 size0 %d\n", queue_size(ready_queue));
  pcb_t * next_pcb_p = (pcb_t*) queue_pop_head(ready_queue);
  //TracePrintf(1, "queue trap0 size %d\n", queue_size(ready_queue));
  if (next_pcb_p != QUEUE_NULL) {
    //TracePrintf(1, "nonempty trap0: curr pc %x\n", curr_pcb_p->u_con.pc);

    TracePrintf(1, "trap0 pc %x sp %x\n", u_con->pc, u_con->sp);
    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
    
    memcpy(&(g_curr_pcb_p->u_con), u_con, sizeof(UserContext));
    //TracePrintf(1, "trap1 1, pc is now %x %x\n", next_pcb_p->pid, curr_pcb_p->pid);
    queue_push_tail(ready_queue, g_curr_pcb_p);
    KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void *) next_pcb_p);
    *u_con = g_curr_pcb_p->u_con;
    //TracePrintf(1, "trap1 2, pc is now %x %x\n", next_pcb_p->pid, curr_pcb_p->pid);
  } 
}

void _trap_illegal(UserContext *u_con) { 
  //free up the pcb corresponding to the user context 
  //==> cant free, a process might be waiting on it. make it defunct and put it in a zombie queue or something
  //clearing registers etc,
  
  //call exit with the illegal code
  TracePrintf(1, "trap illegal with code %x\n", u_con->code);
  Exit(u_con->code);

}

void _trap_memory(UserContext *u_con) { 
  //check if the requested addr is lower than the kernel stack limit + a page
  // NOTE user process cannot access kernel data_segments_to_write
  
  //if so, exit
  //else set bits up to addr to valid and pop frames as needed from frame stack
  
  //TODO check logic again
  // if below stack but above heap, allocate more frames
  //since in trap memory addr was def under stack

  TracePrintf(1, "bla addr %x pc %x\n",u_con->addr, u_con->pc);
  TracePrintf(1, "queuetrap %x\n", free_frames_queue);
  int vpn = ((unsigned int) u_con->addr >> PAGESHIFT) - MAX_PT_LEN;
  int user_brk = (g_curr_pcb_p->brk_index+MAX_PT_LEN) << PAGESHIFT;
  int user_brk_page = ((unsigned int) user_brk >> PAGESHIFT) - MAX_PT_LEN;
  //u_con->ebp = (void *) (VMEM_1_LIMIT - PAGESIZE);

  //TODO update
  int stack_page = MAX_PT_LEN - 1;//((unsigned int) u_con->ebp) >> PAGESHIFT;
  struct pte * r1_pt = (struct pte*) ReadRegister(REG_PTBR1);
  if ((vpn > user_brk_page) && (vpn < stack_page)) {
    int i;
    for (i = stack_page; i >= vpn; i--) {
      if (r1_pt[i].valid == 1)
	continue;
      r1_pt[i].valid = 1;
      r1_pt[i].prot = PROT_READ | PROT_WRITE;
      r1_pt[i].pfn = (int) queue_pop_head(free_frames_queue);   
    }
    for(i = vpn - 1; i >= user_brk_page; i--) {
      if (r1_pt[i].valid == 0)
	continue;
      r1_pt[i].valid = 0;
      r1_pt[i].prot = PROT_READ | PROT_WRITE;
      queue_push_tail(free_frames_queue, (void *) r1_pt[i].pfn);
    }
    WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_1);
    return;
 }
  else {
    //TODO test!

    TracePrintf(1, "ERROR addr requested: %x\n", u_con->addr);
    TracePrintf(1, "ERROR vpn requested: %d\n", vpn);
    
    TracePrintf(1, "ERROR user break page: %d\n", user_brk_page);
    TracePrintf(1, "ERROR stack page: %d\n", stack_page);
    Exit(-1);
    return;
  }
  
}

void _trap_math(UserContext *u_con) { 
  //exit and throw the error code corresponding to trap math
  TracePrintf(1, "trap math with code %x\n", u_con->code);
  Exit(u_con->code);
}

void _trap_tty_receive(UserContext *u_con) { 
  TracePrintf(1, "_trap_tty_received: Starting\n");
  int tty_id = u_con->code;
  char * buf = malloc(TERMINAL_MAX_LINE);
  
  int len = TtyReceive(tty_id, buf, TERMINAL_MAX_LINE);
  buf[len] = '\0';
  TracePrintf(1, "_trap_tty_received: Received %d chars\n", len);
  TracePrintf(1, "_trap_tty_received: strlen %d chars\n", strlen(buf));

  queue_push_tail(data_segments_to_receive[tty_id], buf);
  pcb_t* next_pcb_p = (pcb_t*) queue_pop_head(waiting_to_read[tty_id]);
  if (next_pcb_p != QUEUE_NULL)
    queue_push_tail(ready_queue, next_pcb_p);
}

void _trap_tty_transmit(UserContext *u_con) { 
  // just add data to transmit queue
  // i assume tty control device constantly reads from this queue
  
  TracePrintf(1, "_trap_tty_transmit: Starting\n");
  int tty_id = u_con->code;
  // if the currently writing process is done writing
  // stick it in the ready queue and change writing pcb_p to NULL
  // also push any waiting to write processes to the ready queue
  if (data_segments_to_transmit[tty_id] == 0 && writing_pcb_p[tty_id] != NULL) {
    queue_push_tail(ready_queue, writing_pcb_p[tty_id]);
    writing_pcb_p[tty_id] = NULL;
    TracePrintf(1, "_trap_tty_transmit: about pop next pcb from waiting to write queue\n");
    
    pcb_t* next_pcb_p = (pcb_t*) queue_pop_head(waiting_to_write_queues[tty_id]);
    if ((void*) next_pcb_p != QUEUE_NULL) {
      TracePrintf(1, "_trap_tty_transmit: pushing next waiting to write process in ready queue\n");
      queue_push_tail(ready_queue, (void*) next_pcb_p);
    }
    TracePrintf(1, "_trap_tty_transmit: finished popping next pcb from waiting to write queue\n"); 
  }
  else
    queue_push_tail(ready_queue, writing_pcb_p[tty_id]);
  // otherwise, we need to call the currently writing pcb again!
  // to finish transmitting
}

void _trap_disk(UserContext *u_con) { 
  // i don't think we have to do this yet
  TracePrintf(1, "trap disk called\n");
}

void _trap_error(UserContext *u_con) { 
  // in case an illegal trap is called alert OS
  TracePrintf(1, "unrecogized trap thrown!\n");
}

/*initializes interrupt vector at bootstrap*/

void init_i_vect() {
  INTERRUPT_VECTOR[ TRAP_KERNEL       ] = _trap_kernel;
  INTERRUPT_VECTOR[ TRAP_CLOCK        ] = _trap_clock;
  INTERRUPT_VECTOR[ TRAP_ILLEGAL      ] = _trap_illegal;
  INTERRUPT_VECTOR[ TRAP_MEMORY       ] = _trap_memory;
  INTERRUPT_VECTOR[ TRAP_MATH         ] = _trap_math;
  INTERRUPT_VECTOR[ TRAP_TTY_RECEIVE  ] = _trap_tty_receive;
  INTERRUPT_VECTOR[ TRAP_TTY_TRANSMIT ] = _trap_tty_transmit;
  INTERRUPT_VECTOR[ TRAP_DISK         ] = _trap_disk;


  //fill unused trap slots with an error function
  int i;
  for (i = TRAP_DISK + 1; i < TRAP_VECTOR_SIZE; i++)
    INTERRUPT_VECTOR[ i ] = _trap_error;
}
