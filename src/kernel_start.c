#include "../include/hardware.h"
#include "../include/trap.h"
#include "../include/yalnix.h"
#include "../algos-and-data-structures/queue.h"
#include "../include/load_program.h"
#include "../include/process.h"

#include <stdlib.h>

unsigned int __KernelDataStart;
unsigned int __KernelDataEnd;

struct pte r0_pt[MAX_PT_LEN];
struct pte r1_pt_init[MAX_PT_LEN];
struct pte r1_pt_idle[MAX_PT_LEN];

void* INTERRUPT_VECTOR[TRAP_VECTOR_SIZE];

Queue* free_frames_queue;

Queue* ready_queue;
Queue* blocked_queue;
Queue* dead_queue;

Queue* waiting_to_write_queues[NUM_TERMINALS];
int data_segments_to_transmit[NUM_TERMINALS];
pcb_t* writing_pcb_p[NUM_TERMINALS];

Queue* data_segments_to_receive[NUM_TERMINALS];
Queue* waiting_to_read[NUM_TERMINALS];

//Queue *sync_queue; //queue to hold synchronization structs
int g_sync_id; //ids for the sync structs

pcb_t init_pcb, idle_pcb;

UserContext *g_u_con;

pcb_t* g_curr_pcb_p;

typedef struct frame frame_t;

// you gotta set the kernel context, otherwise before switching, you'll get bogus contexts
KernelContext *SetKernelContext(KernelContext *kc_in, void *curr_pcb_pter, void* same_pcb_pter) {
  if (curr_pcb_pter != same_pcb_pter) {
    TracePrintf(1, "SetKernelContext got non equal pcb pointers\n");
    return NULL;
  }

  pcb_t* curr_pcb_p = (pcb_t*) curr_pcb_pter;
  memcpy(&(curr_pcb_p->k_con), kc_in, sizeof(KernelContext));

  struct pte* stack_base_pte_p = (struct pte*) curr_pcb_p->reg_ptbr0 + MAX_PT_LEN - 1;
  struct pte* stack_top_pte_p = (struct pte*) curr_pcb_p->reg_ptbr0 + MAX_PT_LEN - 2;
  
  u_long old_base_pfn = stack_base_pte_p->pfn;
  u_long old_top_pfn = stack_top_pte_p->pfn;

  // copy over the base stack
  stack_top_pte_p->pfn = curr_pcb_p->kernel_stack_pfns[0];
  WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_0);
  memcpy((void*)KERNEL_STACK_BASE, (void *) KERNEL_STACK_BASE + PAGESIZE, PAGESIZE);

  // copy over the stack top
  stack_base_pte_p->pfn = curr_pcb_p->kernel_stack_pfns[1];
  stack_top_pte_p->pfn = old_top_pfn;

  WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_0);
  memcpy((void*)KERNEL_STACK_BASE + PAGESIZE, (void *) KERNEL_STACK_BASE, PAGESIZE);
  stack_base_pte_p->pfn = old_base_pfn;
  WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_0);
  
  TracePrintf(1, "setctxt\n");

  return kc_in;
}

KernelContext *MyKCS(KernelContext *kc_in, void *curr_pcb_pter, void *next_pcb_pter) {
  if (curr_pcb_pter == next_pcb_pter) {
    TracePrintf(1, "MyKCS got equal pcb pointers\n");
    return NULL;
  }

  pcb_t* next_pcb_p = ((pcb_t*) next_pcb_pter);

  pcb_t* curr_pcb_p = ((pcb_t*) curr_pcb_pter);
  // keep the current kernel context
  memcpy(&(curr_pcb_p->k_con), kc_in, sizeof(KernelContext));
  TracePrintf(1, "finished copying kernel context\n");
  
  ((struct pte*) curr_pcb_p->reg_ptbr0 + MAX_PT_LEN - 1) -> valid = 1;
  ((struct pte*) curr_pcb_p->reg_ptbr0 + MAX_PT_LEN - 1) -> pfn = next_pcb_p->kernel_stack_pfns[0];

  ((struct pte*) curr_pcb_p->reg_ptbr0 + MAX_PT_LEN - 2) -> valid = 1;
  ((struct pte*) curr_pcb_p->reg_ptbr0 + MAX_PT_LEN - 2) -> pfn = next_pcb_p->kernel_stack_pfns[1];

  WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_0);
  WriteRegister(REG_PTBR1, (unsigned int) next_pcb_p->reg_ptbr1);
  WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_ALL);
  TracePrintf(1, "set registers and flushed tlbs.  done\n");
  
  //dont use pcb kernel contexts direclty. copy them into the current kernel context
  //memcpy(kc_in, &(next_pcb_p->k_con), sizeof(KernelContext));
  g_curr_pcb_p = next_pcb_p;
  return &(next_pcb_p->k_con);

}

void idle() {
  TracePrintf(1, "starting idle\n");

  while(1)
  {
    TracePrintf(1, "%p running idle\n", Pause);
    Pause();
    TracePrintf(1, "paused idle\n");
  }
  TracePrintf(1, "stopping idle\n");
} 

void SetKernelData(void *KernelDataStart, void *KernelDataEnd) {
  TracePrintf(1, "Starting SetKernelData\n");
  __KernelDataStart = (unsigned int) KernelDataStart;
  __KernelDataEnd = (unsigned int) KernelDataEnd;
  TracePrintf(1, "Ending SetKernelData\n");
}

int SetKernelBrk(void *addr) {
  TracePrintf(1, "Starting SetKernelBrk\n");
  if(!ReadRegister(REG_VM_ENABLE)) {
      TracePrintf(1, "VM is currently disabled\n");
      __KernelDataEnd = (unsigned int) addr;
      return 0;
    }
  unsigned int vpn = (unsigned int) addr >> PAGESHIFT;
  // check if valid addr in kernel heap
  if ((vpn >= (VMEM_0_LIMIT >> PAGESHIFT) - 3) || (vpn < VMEM_0_BASE >> PAGESHIFT)){
    return ERROR;
  }
  int i;
  for (i = 0; i <= vpn; i++) {
    if (r0_pt[i].valid == 1)
      continue;
    r0_pt[i].valid = 1;
    r0_pt[i].prot = PROT_READ | PROT_WRITE;
    r0_pt[i].pfn = (unsigned int) queue_pop_head(free_frames_queue);
  }
  for(i = vpn + 1; i < (VMEM_0_LIMIT>>PAGESHIFT) - 3; i++) {
    if (r0_pt[i].valid == 0)
      continue;
    r0_pt[i].valid = 0;
    r0_pt[i].prot = PROT_READ | PROT_WRITE;
    queue_push_tail(free_frames_queue, (void *) NULL + r0_pt[i].pfn);
  }
  return 0;
}

void KernelStart(char *cmd_args[], unsigned int pmem_size, UserContext *uctxt) {
  TracePrintf(1, "Starting KernelStart\n");
  
  TracePrintf(1, "Initializing Interrupt Vector Table\n");
  init_i_vect();
  WriteRegister(REG_VECTOR_BASE, (int) INTERRUPT_VECTOR);
  TracePrintf(1, "Seems like we successfully initialized Interrupt Vector Table\n");
  
  TracePrintf(1, "Initializing Frame Queue\n");  
  free_frames_queue = queue_new();
  int i;
  for(i = 0; i < pmem_size >> PAGESHIFT; i++) {
    queue_push_tail(free_frames_queue, (void *) i);
  }
  
  int start_text_frame_num = (int) __KernelDataStart >> PAGESHIFT;
  int end_text_frame_num = (int) __KernelDataEnd >> PAGESHIFT;
  int start_stack_frame_num = (int) KERNEL_STACK_BASE >> PAGESHIFT;  
  TracePrintf(1, "end frame num: %d, total frames: %d\n", end_text_frame_num, pmem_size >> PAGESHIFT);

  TracePrintf(1, "Done initializing Frame Queue.  Initializing R0\n");
  for(i = 0; i < start_text_frame_num; i++) {
    r0_pt[i].valid = 1;
    r0_pt[i].prot = PROT_READ | PROT_EXEC;
    r0_pt[i].pfn = (int)queue_pop_head(free_frames_queue);
  }
  
  for(i = start_text_frame_num; i < end_text_frame_num; i++) {
    r0_pt[i].valid = 1;
    r0_pt[i].prot = PROT_READ | PROT_WRITE;
    r0_pt[i].pfn = (int)queue_pop_head(free_frames_queue);;
  } 
  
  // stuff between r0 text and stack
  for(i = end_text_frame_num; i < MAX_PT_LEN; i++) {
    r0_pt[i].valid = 0;
    r0_pt[i].prot = PROT_READ | PROT_WRITE;
  }

  // r0 stack
  for(i = start_stack_frame_num; i < MAX_PT_LEN; i++) {
    r0_pt[i].valid = 1;
    r0_pt[i].prot =  PROT_READ | PROT_WRITE;
    r0_pt[i].pfn = i;

    TracePrintf(1, "stack frame %d\n", i);

    int pfn = (int) queue_pop_head(free_frames_queue);
    while(pmem_size >> PAGESHIFT - 1 != pfn) {
      if (i == pfn)
	break;
      queue_push_tail(free_frames_queue, (void *) pfn);
      pfn = (int) queue_pop_head(free_frames_queue);
    }
  }
  
  
  for(i = 0; i < MAX_PT_LEN; i++) {
    r1_pt_init[i].valid = 0;
    r1_pt_init[i].prot = PROT_READ | PROT_WRITE;
  }
  r1_pt_init[MAX_PT_LEN - 1].valid = 1;

  r1_pt_init[MAX_PT_LEN - 1].pfn = (int) queue_pop_head(free_frames_queue);

  for(i = 0; i < MAX_PT_LEN; i++) {
    r1_pt_idle[i].valid = 0;
    r1_pt_idle[i].prot = PROT_READ | PROT_WRITE;
  }
  r1_pt_idle[MAX_PT_LEN - 1].valid = 1;
  r1_pt_idle[MAX_PT_LEN - 1].pfn = (int) queue_pop_head(free_frames_queue);
  r1_pt_idle[MAX_PT_LEN - 1].prot = PROT_READ | PROT_WRITE;

 
  WriteRegister(REG_PTBR0, (unsigned int) r0_pt);
  WriteRegister(REG_PTBR1, (unsigned int) r1_pt_init);
  WriteRegister(REG_PTLR0, MAX_PT_LEN);
  WriteRegister(REG_PTLR1, MAX_PT_LEN);
  
  ready_queue = queue_new();
  dead_queue = queue_new();
  blocked_queue = queue_new();

  int k;
  for(k = 0; k < NUM_TERMINALS; k++) {
    waiting_to_write_queues[k] = queue_new();
    data_segments_to_receive[k] = queue_new();
    waiting_to_read[k] = queue_new();
  }


  WriteRegister(REG_VM_ENABLE, 1);
  TracePrintf(1, "Reg VM enabled\n");

  g_process_count = 1;

  idle_pcb.pid = g_process_count++;
  idle_pcb.u_con = *uctxt;
  idle_pcb.u_con.pc = idle;
  idle_pcb.u_con.sp = (void*) VMEM_1_LIMIT - PAGESIZE;
  idle_pcb.u_con.ebp = (void*) VMEM_1_LIMIT - PAGESIZE;
  idle_pcb.reg_ptbr0 = r0_pt;
  idle_pcb.reg_ptlr0 = (void*) MAX_PT_LEN;
  idle_pcb.reg_ptbr1 = r1_pt_idle;
  idle_pcb.reg_ptlr1 = (void*) MAX_PT_LEN;
  idle_pcb.kernel_stack_pfns[0] = (u_long) queue_pop_head(free_frames_queue);
  idle_pcb.kernel_stack_pfns[1] = (u_long) queue_pop_head(free_frames_queue);
  //init sync stuff
  idle_pcb.sync_queue = queue_new();
  idle_pcb.sync_id = 1;
  
  queue_push_tail(ready_queue, &idle_pcb);

  init_pcb.pid = g_process_count++;
  init_pcb.u_con = *uctxt;
  init_pcb.reg_ptbr0 = r0_pt;
  init_pcb.reg_ptlr0 = (void*) MAX_PT_LEN;
  init_pcb.reg_ptbr1 = r1_pt_init;
  init_pcb.reg_ptlr1 = (void*) MAX_PT_LEN;
  init_pcb.kernel_stack_pfns[0] = MAX_PT_LEN-1;
  init_pcb.kernel_stack_pfns[1] = MAX_PT_LEN-2;
    //init sync stuff
  init_pcb.sync_queue = queue_new();
  init_pcb.sync_id = 1;
  g_curr_pcb_p = &init_pcb;
  
  TracePrintf(1, "about to Load Program\n");
  int rc = LoadProgram(cmd_args[0], cmd_args, &init_pcb);
  if (rc!=1) {
    TracePrintf(1, "LoadProgram failed\n");
    Halt();
  }

  TracePrintf(1, "KernelStart: LoadProgram returned %d\n", rc);
  

  TracePrintf(1, "ucon brk: %x\n", uctxt->addr);
  KernelContextSwitch(SetKernelContext, (void*) &init_pcb, (void*) &init_pcb);
  
  KernelContextSwitch(SetKernelContext, (void*) &idle_pcb, (void*) &idle_pcb);
  
  *uctxt = g_curr_pcb_p->u_con;
  
}


/*//copy kernel stacks for parent and child
KernelContext *CpKernelContext(KernelContext *kc_in, void *curr_pcb_pter, void* next_pcb_pter) {
  pcb_t* parent = (pcb_t*) curr_pcb_pter;
  pcb_t* child = (pcb_t*) next_pcb_pter;
  int addr;

  TracePrintf(1, "copying kernel context from parent to child\n!");
  memcpy(&(child->k_con), kc_in, sizeof(KernelContext));

  //swithc pfns and copy stuff
  struct pte* p_top = ((struct pte*) parent->reg_ptbr0 + MAX_PT_LEN - 1);
  struct pte* p_bot = ((struct pte*) parent->reg_ptbr0 + MAX_PT_LEN - 2) ;

  //funky funky ****
  p_top->valid = 1;
  addr = p_top->pfn;
  p_top->pfn = child->kernel_stack_pfns[1]; //bottom
  WriteRegister(REG_TLB_FLUSH, (MAX_PT_LEN-1)<<PAGESHIFT);
  memcpy( (void*) ((MAX_PT_LEN-1)<<PAGESHIFT), (void*) ((MAX_PT_LEN-2)<<PAGESHIFT), PAGESIZE); //copy parent bottom to child bottom pfn
  p_top->pfn = addr;
  WriteRegister(REG_TLB_FLUSH, (MAX_PT_LEN-1)<<PAGESHIFT);

  p_bot->valid = 1;
  addr = p_bot->pfn;
  p_bot->pfn = child->kernel_stack_pfns[0]; //top
  WriteRegister(REG_TLB_FLUSH, (MAX_PT_LEN-2)<<PAGESHIFT);
  memcpy( (void*) ((MAX_PT_LEN-2)<<PAGESHIFT), (void*) ((MAX_PT_LEN-1)<<PAGESHIFT), PAGESIZE); //copy parent top to child top
  p_bot->pfn = addr;
  WriteRegister(REG_TLB_FLUSH, (MAX_PT_LEN-2)<<PAGESHIFT);

  return kc_in;
  }*/


