#include "../include/sys_calls.h"


int GetPid() {
  pcb_t *curr = g_curr_pcb_p;
  if (curr != NULL)
    return curr->pid;
  
  else
    return -1; //what?
}

int Delay(int clock_ticks) {
  if (clock_ticks < 0)
    return ERROR;

  if (clock_ticks == 0)
    return 0;
  
  TracePrintf(1, "Delay call with %x \n", clock_ticks);
  pcb_t *curr = g_curr_pcb_p;
  pcb_t *next = (pcb_t*) queue_pop_head(ready_queue);

  curr->block_ticks = clock_ticks;

  TracePrintf(1, "queue size01 %d\n", queue_size(ready_queue));
  
  memcpy(&(curr->u_con), g_u_con, sizeof(UserContext));
  //TracePrintf(1, "g_u_con pc %x \n", g_u_con->pc);
  queue_push_tail(blocked_queue, curr);
  KernelContextSwitch(SetKernelContext, (void*) curr, (void*) curr);
  KernelContextSwitch(MyKCS, (void*) curr, (void *) next);
  TracePrintf(1, "Delay: returned from Delay!\n");
  memcpy(g_u_con, &(g_curr_pcb_p->u_con), sizeof(UserContext));

  //TracePrintf(1, "u_con pc is now %x\n", g_u_con->pc);
  //wait

  //TracePrintf(1, "queue size02 %d\n", queue_size(ready_queue));
  return 0;
}

int Brk(void *addr) {
  TracePrintf(1, "Brk: called brk on addr %x!\n", addr);
  
  //next brk_index
  unsigned int b = ((unsigned int) addr >> PAGESHIFT) - MAX_PT_LEN;

  pcb_t *curr = g_curr_pcb_p;
 
  TracePrintf(1, "Brk: moving brk from %u to %u!\n", curr->brk_index, b);
  
  //if addr is in r0 or in the stack return error
  TracePrintf(1, "Brk: sp page %u\n", DOWN_TO_PAGE(curr->u_con.sp) >> PAGESHIFT);
  int i;
  struct pte *temp;
  if (b + MAX_PT_LEN >= ((unsigned int) curr->u_con.sp >> PAGESHIFT) || addr < VMEM_1_BASE) {
    TracePrintf(1, "Brk: tried to allocate in stack or below r1\n");
    TracePrintf(1, "Brk: sp %x addr: %x\n", curr->u_con.sp, addr);
    return ERROR;
  }
  else if (b < curr->brk_index) { //dealloc
    TracePrintf(1, "deallocating brk to %d!\n", b);
    for (i = curr->brk_index; i > b-1; i--) {
      temp = &curr->reg_ptbr1[i];
      if (temp->valid == 1) {
	temp->valid = 0;
	queue_push_tail(free_frames_queue, (void*) temp->pfn);
	temp->prot = 0;
      }
      //else {
      //TracePrintf(1, "weird dealloc brk2 %d!\n", i);
      //return ERROR;
      //}
    }
    
    WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_ALL);
    curr->brk_index = b;
    return 0;
  }
  
  else { //allocate
    TracePrintf(1, "allocating brk %u %d!\n", b, curr->brk_index);
    for (i = curr->brk_index; i < b; i++) {
      //TracePrintf(1, "allocating brk %d!\n", i);
      temp = &curr->reg_ptbr1[i];
      if (temp->valid == 0) {
	temp->valid = 1;
	temp->pfn = (unsigned int) queue_pop_head(free_frames_queue);
	temp->prot = PROT_WRITE | PROT_READ;
	//      TracePrintf(1, "allocating brk2 %d!\n", i);
      }
      //else {
      //TracePrintf(1, "weird brk2 %d!\n", i);
      //return ERROR;
      //}
    }
    WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_ALL);
    curr->brk_index = b;
    return 0;
  }
}


int Exec(char *filename, char **argvec) {
  TracePrintf(1, "Exec: starting\n");
  TracePrintf(1, "Exec: executing filename = %s\n", filename);
  pcb_t *top = g_curr_pcb_p;
  int rc = LoadProgram(filename, argvec,  top);
  *g_u_con = g_curr_pcb_p->u_con;
  TracePrintf(1, "load returned %d\n", rc);

  if (rc == ERROR) {
    TracePrintf(1, "Exec: LoadProgram returned with error sig\n");
    return ERROR;
  }

  if (rc == KILL) {
    TracePrintf(1, "Exec: LoadProgram returned with kill sig\n");
    Exit(-1);
    /*//kill this process
    top = queue_pop_head(ready_queue);
    e_stat_t *ex = malloc(sizeof(e_stat_t));

    //TODO
    //free frames
    int i;
    for(i = 0; i < MAX_PT_LEN; i++){
      if(top->reg_ptbr1[i].valid == 1)
	queue_push_tail(free_frames_queue, top->reg_ptbr1[i].pfn);
    }


    if (ex == NULL)
      return ERROR;
    
    ex->pid = top->pid;
    ex->exit_status = rc;
    queue_push_tail(dead_queue, (void*) ex);

    free(top);
    
    return ERROR;*/
  }
  WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_ALL);
  return ERROR;
}

int Fork() {
  TracePrintf(1, "Fork: in Fork %d!\n", queue_size(free_frames_queue));
  pcb_t *parent = g_curr_pcb_p;
  pcb_t *child = malloc(sizeof(pcb_t));
  if (child == NULL) {
    TracePrintf(1, "Fork: couldn't malloc child pcb\n");
    return ERROR; 
  }
  memset(child, 0, sizeof(pcb_t));

  //char tmp[PAGESIZE]; Sean says "no no"
  int addr;
  int empty_addr, empty_i;
  int i;

  //allocate same amout of pages and copy their content over
  child->reg_ptbr0 = ReadRegister(REG_PTBR0);
  child->reg_ptbr1 = malloc(sizeof(struct pte)*MAX_PT_LEN);

  TracePrintf(1, "in Fork %d!\n", queue_size(free_frames_queue));

  if (child->reg_ptbr0 == NULL || child->reg_ptbr1 == NULL)
    return ERROR;
  //TracePrintf(1, "in Fork %d!\n", queue_size(free_frames_queue));
  memset(child->reg_ptbr1, 0, sizeof(struct pte)*MAX_PT_LEN);

  //TracePrintf(1, "in Fork %d!\n", queue_size(free_frames_queue));

  child->reg_ptlr0 = (void *) MAX_PT_LEN;
  child->reg_ptlr1 = (void *) MAX_PT_LEN;

  //copy over all page tables
  //save contexts
  //copy over contexts and pcb info
  child->pid = g_process_count++;
  child->u_con = parent->u_con;
  child->k_con = parent->k_con;
  child->parent = parent;
  child->brk_index = parent->brk_index;
  child->block_ticks = parent->block_ticks;
  child->wait_for_child = parent->wait_for_child;
  child->sync_id = parent->sync_id;
  child->sync_queue = queue_new();
  
  // copy over sync queue
  QueueEntry *curr = g_curr_pcb_p->sync_queue->head;
  while (curr != NULL) {
    queue_push_tail(child->sync_queue, curr->data);
    curr = curr->next;
  }


  if (parent->children == NULL) 
    parent->children = queue_new();
  if (parent->child_statuses == NULL)
    parent->child_statuses = queue_new();
  queue_push_tail(parent->children, (void*) child);
  //queue_push_tail(ready_queue, (void *) child);

  //find unused memory in R1
  for (i = 0; i < MAX_PT_LEN; i++) {
    if (parent->reg_ptbr1[i].valid == 0) {
      parent->reg_ptbr1[i].valid = 1;
      parent->reg_ptbr1[i].pfn = queue_pop_head(free_frames_queue);
      parent->reg_ptbr1[i].prot = PROT_WRITE | PROT_READ;
      
      empty_i = i;
      empty_addr = (i+MAX_PT_LEN)<<PAGESHIFT;
      TracePrintf(1, "found empty mem for child %d %x %d!\n", empty_i, parent->reg_ptbr1[i].pfn, queue_size(free_frames_queue));
      break;
    }
  }
  

  TracePrintf(1, "in Fork copying ptes!\n");
  for (i = 0; i < MAX_PT_LEN; i++) {
    //copy over R1
    if ((parent->reg_ptbr1[i].valid) && (i != empty_i)){
      child->reg_ptbr1[i].valid = 1;
      child->reg_ptbr1[i].prot = parent->reg_ptbr1[i].prot;
      child->reg_ptbr1[i].pfn = parent->reg_ptbr1[empty_i].pfn;
      
      TracePrintf(1, "in Fork copying to temp %x!\n", child->reg_ptbr1[i].pfn);  
      // child's i-th pfn equals the empty slot
      memcpy((void*) empty_addr, (void*)((i+MAX_PT_LEN)<< PAGESHIFT), PAGESIZE);
      parent->reg_ptbr1[empty_i].pfn = queue_pop_head(free_frames_queue);
      WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_1);
      
      }
    else {
      child->reg_ptbr1[i].valid = 0;
    }
  }

  //dealloc empty addr
  parent->reg_ptbr1[empty_i].valid = 0;
  parent->reg_ptbr1[empty_i].prot = 0;
  queue_push_head(free_frames_queue, (void*) (parent->reg_ptbr1[empty_i].pfn));
  parent->reg_ptbr1[empty_i].pfn = 0;

  TracePrintf(1, "in Fork copied ptes!\n");
  
  //copy kernel stack!
  child->kernel_stack_pfns[0] = queue_pop_head(free_frames_queue);
  child->kernel_stack_pfns[1] = queue_pop_head(free_frames_queue);
  
  queue_push_tail(ready_queue, child);

  KernelContextSwitch(SetKernelContext, (void*) child, (void*) child);
  
  TracePrintf(1, "in Fork copied contexts %x!\n", GetPid());
  
  if (GetPid() == parent->pid) {
    TracePrintf(1, "in Fork parent!!\n");
    return child->pid;
  }
  else {
    TracePrintf(1, "Fork: in Fork child 0!!\n");
    return 0;
  }
}

void Exit(int status) {
  // I let trap_clock move the parent from the blocked queue to the ready queue 
  TracePrintf(1, "Exit: pid %d, pcb addr %x, status %d\n", GetPid(), g_curr_pcb_p, status);
  pcb_t* curr_pcb_p = g_curr_pcb_p;
  
  if (curr_pcb_p == &init_pcb)
    {
      TracePrintf(1, "Halting\n");
      Halt();
    }

  

  pcb_t* parent_p = curr_pcb_p->parent;
  // let parent know about child status
  if (parent_p != NULL){
    TracePrintf(1, "Exit: turning off wait for child flag\n");
    parent_p->wait_for_child = 0;
    e_stat_t* exit_status = malloc(sizeof(e_stat_t));
    exit_status->pid = curr_pcb_p->pid;
    exit_status->exit_status = status;
    queue_push_tail(parent_p->child_statuses, exit_status);
    // remove child from parent's list of children
    // this needs to be cleaned up substantially
    while(queue_peek_head(parent_p->children) != curr_pcb_p)
      queue_push_tail(parent_p->children, queue_pop_head(parent_p->children));
    queue_pop_head(parent_p->children);
  }
  
  // switch processes
  pcb_t* next_pcb_p = queue_pop_head(ready_queue);   
  *g_u_con = next_pcb_p->u_con;

    // free resources
  queue_push_tail(free_frames_queue, curr_pcb_p->kernel_stack_pfns[0]);
  queue_push_tail(free_frames_queue, curr_pcb_p->kernel_stack_pfns[1]);
  free(curr_pcb_p);
  KernelContextSwitch(MyKCS, (void *) curr_pcb_p, (void *) next_pcb_p);
}

int Wait(int * status_ptr) {
  TracePrintf(1, "Wait: Starting\n");
  if ( (unsigned int) status_ptr < VMEM_1_BASE || status_ptr >= VMEM_1_LIMIT)
    {
      TracePrintf(1, "Wait: status ptr below r1 or above VMEM1LIMIT.  addr: %x page: %u\n", status_ptr, (unsigned int)status_ptr >> PAGESHIFT);
      return ERROR;
    }
  while(1) 
    {
      TracePrintf(1, "Wait: in while loop\n");
      if (g_curr_pcb_p->child_statuses == NULL || queue_is_empty(g_curr_pcb_p->child_statuses))
	{
	  TracePrintf(1, "Wait: No child statuses\n");
	  if (g_curr_pcb_p->children == NULL || queue_is_empty(g_curr_pcb_p->children))
	    {
	      TracePrintf(1, "Wait: No children.  Returning Error\n");
	      return ERROR;
	    }
	  else {
	    TracePrintf(1, "Wait: waiting for child\n");
	    queue_push_tail(blocked_queue, g_curr_pcb_p);  
	    g_curr_pcb_p->wait_for_child = 1;
	    pcb_t* next_pcb_p = queue_pop_head(ready_queue);
	    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
	    KernelContextSwitch(MyKCS, (void *) g_curr_pcb_p, (void *) next_pcb_p);
	    *g_u_con = g_curr_pcb_p->u_con;
	    TracePrintf(1, "Wait: switching from Wait\n");
	  }
	}
      else 
	{
	  TracePrintf(1, "Wait: returning exit status PID\n");
	  e_stat_t *e_stat = queue_pop_head(g_curr_pcb_p->child_statuses);
	  *status_ptr = e_stat->exit_status;
	  TracePrintf(1, "Wait: exit status %d\n", e_stat->exit_status);
	  return e_stat->pid;
	}
    }
}

int TtyRead(int tty_id, void *buf, int len) {
  TracePrintf(1, "TtyRead: Starting\n");
  if (buf == NULL || len < 0 || tty_id < 0 || tty_id >= NUM_TERMINALS)
    return ERROR;

  char* data = queue_pop_head(data_segments_to_receive[tty_id]);
  // if there are no data segments, block
  if(data == NULL) {
    queue_push_tail(waiting_to_read[tty_id], g_curr_pcb_p);
    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
    (g_curr_pcb_p->u_con) = *g_u_con;
    pcb_t* next_pcb_p = queue_pop_head(ready_queue);
    TracePrintf(1, "TtyRead: Waiting for currently reading process to finish\n");
    KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void*) next_pcb_p);    
    data = queue_pop_head(data_segments_to_receive[tty_id]);
  }
  
  TracePrintf(1, "TtyRead: Proceeding to read %d chars\n", len);
  TracePrintf(1, "TtyRead: data=%s\n", data);
      
  // consume all chars if data has fewer characters than len
  if(strlen(data) <= len) {
    TracePrintf(1, "TtyRead: Consuming all chars since data has %d chars less than or equal to len\n", strlen(data));
    memcpy(buf, data, strlen(data));
    free(data);
    return strlen(data);
  }
  else {
    TracePrintf(1, "TtyRead: Consuming only %d chars of data which has len %d\n", len, strlen(data));
    //consume only len chars from data and stick it back in
    memcpy(buf, data, len);
    char* new_data = malloc(strlen(data)-len+1);
    memcpy(new_data, data + len, strlen(data)-len);
    new_data[strlen(data)-len] = '\0';
    TracePrintf(1, "TtyRead: new data: %s\n", new_data);
    free(data);
    queue_push_head(data_segments_to_receive[tty_id], new_data);
    return len;
  }
  return 0;
}

int TtyWrite(int tty_id, void *buf, int len) {
  TracePrintf(1, "TtyWrite: TtyWrite starting\n");
  if (buf == NULL || len < 0 || tty_id < 0 || tty_id >= NUM_TERMINALS)
    return ERROR;

  // if someone's writing, wait
  while(writing_pcb_p[tty_id] != NULL) {
    queue_push_tail(waiting_to_write_queues[tty_id], g_curr_pcb_p);
    // block and such
    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
    (g_curr_pcb_p->u_con) = *g_u_con;
    pcb_t* next_pcb_p = queue_pop_head(ready_queue);
    TracePrintf(1, "TtyWrite: Waiting for currently writing process to finish %s\n", buf);
    KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void*) next_pcb_p);   
    *g_u_con = g_curr_pcb_p->u_con;
  }
  writing_pcb_p[tty_id] = g_curr_pcb_p;

  TracePrintf(1, "TtyWrite: setting current writing process\n");
    
  // if len is a multiple, we just send an extra segment of len 0.
  data_segments_to_transmit[tty_id] = len/TERMINAL_MAX_LINE+1;

  int i;
  for(i = 0; i < len/TERMINAL_MAX_LINE; i++)
    {
      TtyTransmit(tty_id, buf + i * TERMINAL_MAX_LINE, TERMINAL_MAX_LINE);
      
      KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
      (g_curr_pcb_p->u_con) = *g_u_con;
      pcb_t* next_pcb_p = queue_pop_head(ready_queue);
      data_segments_to_transmit[tty_id]--;
      TracePrintf(1, "TtyWrite: Transmitting %d-th segment\n", i);
      KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void*) next_pcb_p);
      *g_u_con = g_curr_pcb_p->u_con;
    }
  TracePrintf(1, "TtyWrite: Sending last segment\n");
  TtyTransmit(tty_id, buf+i*TERMINAL_MAX_LINE, len-i*TERMINAL_MAX_LINE);
  
  //block and such
  KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
  (g_curr_pcb_p->u_con) = *g_u_con;
  pcb_t* next_pcb_p = queue_pop_head(ready_queue); 
  data_segments_to_transmit[tty_id]--;
  TracePrintf(1, "TtyWrite: waiting for last segment to finish transmitting\n");
  KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void*) next_pcb_p);
  *g_u_con = g_curr_pcb_p->u_con;
  TracePrintf(1, "TtyWrite: returning ttyid %d\n", tty_id);
  return len;
}

int PipeInit(int *pipe_p) {
  //create a new pipe, save it in a queue of pipes, locks and cvars
  //the queue lives in global since it needs to be accessible by different processes
  TracePrintf(1, "PipeInit: Starting\n");
  
  if (pipe_p < VMEM_1_BASE || pipe_p >= VMEM_1_LIMIT)
    return ERROR;
  
  sync_str_t *np = malloc(sizeof(sync_str_t));
  pipe_t *pipe = malloc(sizeof(pipe_t));
  Queue *data = queue_new();
  
  if (np == NULL || data == NULL || pipe == NULL) 
    return ERROR;

  np->id = g_sync_id++;
  np->type = PIPE;
  np->str = pipe;
  pipe->data = data;
  pipe->waiting_to_read = queue_new();
  *pipe_p = np->id;
  queue_push_tail(g_curr_pcb_p->sync_queue, np);
  return 0;
}

//find a sync struct using its id from the common queue
void* sync_queue_find(Queue *sq, int id) {
  sync_str_t *str = NULL;
  QueueEntry *curr = g_curr_pcb_p->sync_queue->head;
  while (curr != NULL) {
    str = (sync_str_t *)curr->data;
    if (str->id == id)
      return str;
    else
      str = NULL;
    curr = curr->next;
  }
  return str;
}

int PipeRead(int pipe_id, void *buf, int len) {
  TracePrintf(1, "PipeRead: Starting\n");
  if (len < 0)
    return ERROR;
  if (len == 0)
    return 0;
  if (buf < VMEM_1_BASE || buf >= VMEM_1_LIMIT)
    return ERROR;
  //read from a pipe for the specified len until there is data
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, pipe_id);
  if (sync_node == NULL || sync_node->type != PIPE)
    return ERROR;

  pipe_t *pipe = (pipe_t*) sync_node->str;

  char *reader;
  pipe_data_t *pdata = (pipe_data_t*) queue_pop_head(pipe->data);
  int amt_read = 0;
  while (len != 0) {	
    TracePrintf(1, "PipeRead: entering while loop\n");
    if (pdata == NULL) {
      TracePrintf(1, "PipeRead: blocking\n");
      pcb_t* next = (pcb_t*) queue_pop_head(ready_queue);
      g_curr_pcb_p->u_con = *g_u_con;
      KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
      TracePrintf(1, "PipeRead: pushing to pipe->waiting to read\n");
      queue_push_tail(pipe->waiting_to_read, g_curr_pcb_p);
      TracePrintf(1, "PipeRead: switching\n");
      KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void*) next);
      *g_u_con = g_curr_pcb_p->u_con;	 
      pdata = (pipe_data_t*) queue_pop_head(pipe->data);
      continue;
    }
    //does this buffer have enough data
    //if so copy as much as requested into specified buffer
    if (len < (pdata->size - pdata->lseek)) {
      TracePrintf(1, "PipeRead: pipe partial read from buffer\n");
      //copy from current location in buf
      reader = pdata->buf + pdata->lseek; 

      memcpy(buf + amt_read, reader, len);
      
      //update current buffer location
      pdata->lseek += len; 
      queue_push_head(pipe->data, pdata);
      break;
    }
    
    //otherwise read as much as you can from here and keep looking
    //free buffer after it is empty
    else {
      TracePrintf(1, "PipeRead: pipe full read from buffer starting\n");
      reader = pdata->buf + pdata->lseek;
      memcpy(buf + amt_read, reader, pdata->size - pdata->lseek);
      TracePrintf(1, "PipeRead: pipe full read from buffer memcpy finished\n");
      len -= (pdata->size - pdata->lseek);
      amt_read += (pdata->size -pdata->lseek);
      free(pdata->buf);
      free(pdata);
      TracePrintf(1, "PipeRead: pipe full read from buffer free successfull\n");
      pdata = (pipe_data_t*) queue_pop_head(pipe->data);
      TracePrintf(1, "PipeRead: pipe full read from buffer done\n");
      TracePrintf(1, "PipeRead: data is currently:%s\n", buf);
    }
  }
 
  return 0;
}

int PipeWrite(int pipe_id, void *buf, int len) {
  if (len < 0)
    return ERROR;
  if (len == 0)
    return 0;
  if (buf < VMEM_0_LIMIT || buf >= VMEM_1_LIMIT)
    return ERROR;
  //write to a pipe from the buffer
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, pipe_id);
  if (sync_node == NULL || sync_node->type != PIPE)
    return ERROR;

  pipe_t *pipe = (pipe_t*) sync_node->str;

  pipe_data_t *pdata = malloc(sizeof(pipe_data_t));
  if (pdata == NULL)
    return ERROR;

  pdata->lseek = 0;
  pdata->size = len;
  pdata->buf = malloc(len);
  if (pdata->buf == NULL)
    return ERROR;

  memset(pdata->buf, 0, len);
  memcpy(pdata->buf, buf, len); 

  queue_push_tail(pipe->data, pdata);
  
  TracePrintf(1, "PipeWrite: done writing %d\n", pdata->lseek);
  
  // unblock process waiting to read from this pipe
  pcb_t* blocked = queue_pop_head(pipe->waiting_to_read);
  if (blocked != QUEUE_NULL) {
    TracePrintf(1, "PipeWrite: unblocking process\n");
    queue_push_tail(ready_queue, blocked);
  }
  return 0;
}

int LockInit(int *lock_p) {
  if (lock_p < VMEM_1_BASE || lock_p >= VMEM_1_LIMIT)
    return ERROR;
  TracePrintf(1, "LockInit: Starting\n");
  sync_str_t *sync_node = malloc(sizeof(sync_str_t));
  lock_t *new_lock = malloc(sizeof(lock_t));
  if (new_lock == NULL || sync_node == NULL) {
    TracePrintf(1, "LockInit: couldn't malloc one of sync node or new lock\n");
    return ERROR;
  }
  else {
    //add to the sync queue
    sync_node->id= g_sync_id++;
    sync_node->type = LOCK;
    *lock_p = sync_node->id;
 
    new_lock->held_by = 0;
    new_lock->locked = 0;
    sync_node->str = new_lock;

    queue_push_tail(g_curr_pcb_p->sync_queue, sync_node);
    return 0;
    }
}

int Acquire(int lock_id) {
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, lock_id);
  if (sync_node == NULL || sync_node->type != LOCK)
    return ERROR;

  lock_t *lock = (lock_t*) sync_node->str;
  
  // wait until lock is free
  pcb_t *next;
  int pid = GetPid();
  
  if (pid == lock->held_by)
    return ERROR;

  while (lock->locked != 0) {
    next = (pcb_t*) queue_pop_head(ready_queue);
    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
    queue_push_tail(ready_queue, g_curr_pcb_p);
    KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void *) next);
  }
  
  lock->held_by = pid;
  lock->locked = 1;
  return 0;
}

int Release(int lock_id) {
  TracePrintf(1, "release: about to release lock %d\n", lock_id);
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, lock_id);
  if (sync_node == NULL || sync_node->type != LOCK)
    return ERROR;

  lock_t *lock = (lock_t*) sync_node->str;
  int pid = GetPid();

  TracePrintf(1, "release: releasing lock %d held by %d in process %d lock is locked %d\n", lock_id, lock->held_by, pid, lock->locked);
  if (lock->locked == 0 || lock->held_by != pid) 
    return ERROR;
  
  else {
    lock->held_by = 0;
    lock->locked = 0;
    return 0;
  }
}

int CvarInit(int *cvar_p) {
  if (cvar_p < VMEM_1_BASE || cvar_p >= VMEM_1_LIMIT) {
    return ERROR;
  }
  sync_str_t *sync_node = malloc(sizeof(sync_str_t));
  cvar_t *new_cvar = malloc(sizeof(cvar_t));
  Queue *procs = queue_new();
  if (new_cvar == NULL || sync_node == NULL || procs == NULL) {
    return ERROR;
  }

  else {
    //add to the sync queue
    sync_node->id = g_sync_id++;
    sync_node->type = CVAR;
    *cvar_p = sync_node->id;
 
    new_cvar->procs = procs;
    sync_node->str = new_cvar;

    queue_push_tail(g_curr_pcb_p->sync_queue, sync_node);
    return 0;
  }
}

int CvarWait(int cvar_id, int lock_id) {
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, cvar_id);
  if (sync_node == NULL || sync_node->type != CVAR)
    return ERROR;

  cvar_t *cvar = (cvar_t*) sync_node->str;
  
  //realease lock and block process
  int rc;
  rc = Release(lock_id);
  if (rc == ERROR)
    return ERROR;

  TracePrintf(1, "wait: about to block current process\n");
  pcb_t *next = (pcb_t*) queue_pop_head(ready_queue);
  if (next != QUEUE_NULL) {
    TracePrintf(1, "wait: blocking\n");
    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
    memcpy(&(g_curr_pcb_p->u_con), g_u_con, sizeof(UserContext));
    queue_push_tail(cvar->procs, g_curr_pcb_p);
    KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void *) next);
    *g_u_con = g_curr_pcb_p->u_con;
  }
  TracePrintf(1, "wait: leaving and acquiring data\n");
  Acquire(lock_id);
  return 0;
}

int CvarSignal(int cvar_id) {
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, cvar_id);
  if (sync_node == NULL || sync_node->type != CVAR)
    return ERROR;

  cvar_t *cvar = (cvar_t*) sync_node->str;

  //if anyone is waiting, put them on the ready queue
  pcb_t *next = (pcb_t*) queue_pop_head(cvar->procs);
  if (next != NULL) {
    TracePrintf(1, "cvarsignal: unblocking a process\n");
    queue_push_tail(ready_queue, next);
  }
  
  return 0;
}

int CvarBroadcast(int cvar_id) {
  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, cvar_id);
  if (sync_node == NULL || sync_node->type != CVAR)
    return ERROR;

  cvar_t *cvar = (cvar_t*) sync_node->str;

  //unblock all waiting processes
  pcb_t *next = (pcb_t*) queue_pop_head(cvar->procs);
  while (next != NULL) {
    TracePrintf(1, "cvarbroadcast: ublocking a process\n");
    queue_push_tail(ready_queue, next);
    next = (pcb_t*) queue_pop_head(cvar->procs);
  }
  
  return 0;
}

int SemInit(int *semp_p, int cap) {
  if (semp_p < VMEM_1_BASE || semp_p >= VMEM_1_LIMIT) {
    return ERROR;
  }
  sync_str_t *sync_node = malloc(sizeof(sync_str_t));
  semp_t *new_semp = malloc(sizeof(semp_t));
  Queue *procs = queue_new();
  if (new_semp == NULL || sync_node == NULL || procs == NULL) {
    return ERROR;
  }

  else {
    //add to the sync queue
    sync_node->id = g_sync_id++;
    sync_node->type = SEMP;
    *semp_p = sync_node->id;
 
    new_semp->capacity = cap;
    new_semp->basket = cap;
    new_semp->procs = procs;
    sync_node->str = new_semp;

    queue_push_tail(g_curr_pcb_p->sync_queue, sync_node);
    return 0;
  }

}

int SemUp(int semp_id) {
  TracePrintf(1, "sems: upping %d\n", semp_id);

  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, semp_id);
  if (sync_node == NULL || sync_node->type != SEMP)
    return ERROR;

  semp_t *semp = (semp_t*) sync_node->str;

  if (semp->basket < semp->capacity)
    semp->basket++;

  TracePrintf(1, "sems: upping with b %d c %d p %d\n", semp->basket, semp->capacity, queue_size(semp->procs));

  if (queue_size(semp->procs) > 0) {
    TracePrintf(1, "sems up pushing blocked process to ready queue\n");
    queue_push_tail(ready_queue, (void*) queue_pop_head(semp->procs));
  }

  return 0;
}

int SemDown(int semp_id) {
  TracePrintf(1, "sems: downing %d\n", semp_id);

  sync_str_t *sync_node = sync_queue_find(g_curr_pcb_p->sync_queue, semp_id);
  if (sync_node == NULL || sync_node->type != SEMP)
    return ERROR;

  semp_t *semp = (semp_t*) sync_node->str;

  pcb_t *next;
  if (semp->basket < 1) {
    TracePrintf(1, "sems: waiting for a botton\n");
    next = (pcb_t*) queue_pop_head(ready_queue);
    KernelContextSwitch(SetKernelContext, (void*) g_curr_pcb_p, (void*) g_curr_pcb_p);
    queue_push_tail(semp->procs, (void*) g_curr_pcb_p);
    KernelContextSwitch(MyKCS, (void*) g_curr_pcb_p, (void *) next);
  }

  //if (semp->basket < 1) 
  //  return ERROR;

  semp->basket--;  
  return 0;
}

int Reclaim(int id) {
  pipe_t *pipe;
  pipe_data_t *pdata;
  cvar_t *cvar;
  lock_t *lock;
  semp_t *semp;
  
  sync_str_t *str_node = NULL;
  QueueEntry *curr = g_curr_pcb_p->sync_queue->head;

  while (curr != NULL) {
    str_node = (sync_str_t *)curr->data;
    
    //discover our node
    if (str_node->id == id) 
      break;

    else
      str_node = NULL;
    
    //keep searching
    curr = curr->next;
  }

  //check if found
  if (str_node == NULL)
    return ERROR;

  //if yes then free according to the type
  switch (str_node->type) {

  case PIPE:
    pipe = (pipe_t*) str_node->str;

    //free even if there is data in the pipe
    while (!queue_is_empty(pipe->data)) {
      pdata = (pipe_data_t*) queue_pop_head(pipe->data);
      free(pdata->buf);
      free(pdata);
    }
    
    queue_free(pipe->data);
    free(pipe);
    break;

  case LOCK:
    TracePrintf(1, "Reclaim: Lock: starting\n");
    lock = (lock_t*) str_node->str;
    
    //if lock is claimed fail
    if (lock->locked && lock->held_by != GetPid()) {
      return ERROR;
    }

    free(lock);
    break;
    
  case CVAR:
    cvar = (cvar_t*) str_node->str;

    //break if there are blocked processes
    if (!queue_is_empty(cvar->procs)) {
      return ERROR;
    }
        
    queue_free(cvar->procs);
    free(cvar);
    break;

  case SEMP:
    semp = (semp_t*) str_node->str;

    //break if there are blocked processes
    if (!queue_is_empty(semp->procs)) {
      return ERROR;
    }
        
    queue_free(semp->procs);
    free(semp);
    break;

  }

  //free the wrapper node
  free(str_node);

  //delete from queue of synchronization structures
  if (curr->prev != NULL)  curr->prev->next = curr->next;
  else g_curr_pcb_p->sync_queue->head = curr->next;
  if (curr->next != NULL)  curr->next->prev = curr->prev;
  else g_curr_pcb_p->sync_queue->tail = curr->prev;
  TracePrintf(1, "removed item from linked list\n");
  free(curr);
   
  TracePrintf(1, "reclaim: Reclaim succeeded\n");

  return 0;
}
