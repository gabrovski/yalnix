
/*
 * An init process implementation
 */

#include "include/hardware.h"
#include "include/yalnix.h"


#include <stdlib.h>
//#include <unistd.h>

int main (int argc, char *argv[]) {
  int rc;
  int pipe_id;
  char *test = "the mountains sing an outlaw song";
  char buf[10];
  TracePrintf(1, "init: starting main\n");

  rc = PipeInit(&pipe_id);
  TracePrintf(1, "init: created pipe %d with rc %d\n", pipe_id, rc);

  rc = PipeWrite(pipe_id, test, 25);
  TracePrintf(1, "init: wrote to pipe with rc %d\n", rc);

  int lock;
  LockInit(&lock);
  TracePrintf(1, "init: created lock %x\n", lock);

  int cvar;
  CvarInit(&cvar);
  TracePrintf(1, "init: created cvar %x\n", cvar);

  rc = Fork();
  TracePrintf(1, "init: returned from fork\n");


  int lock2, cvar2, pipe2;
  LockInit(&lock2);
  CvarInit(&cvar2);
  PipeInit(&pipe2);

  Reclaim(lock2);
  Reclaim(cvar2);
  Reclaim(pipe2);
  
  if (rc !=0) {
    rc = Fork();
    if (rc != 0) {
      while(1)  {
	//TracePrintf(1, "init: parent pausing %d\n");
	Pause();
	
	CvarBroadcast(cvar);
	TracePrintf(1, "init: parent broadcasted and waiting\n");
	Acquire(lock);
	CvarWait(cvar, lock);
	TracePrintf(1, "init: parent was signalled by a child\n");
      }
    }

    else {
      while(1)  {
	//TracePrintf(1, "init: child 2 pausing %d\n");
	Pause();

	CvarSignal(cvar);
	TracePrintf(1, "init: child 2 signalled and waiting\n");

	Acquire(lock);
	CvarWait(cvar, lock);
	TracePrintf(1, "init: child 2 was signalled\n");
	
      }

    }

  }
  else {
    PipeRead(pipe_id, buf, 31);

    //TracePrintf(1, "init: returned from fork\n");
    while(1)  {
      //TracePrintf(1, "init: child pausing but read %s\n", buf);
      Pause();
      
      TracePrintf(1, "init: child 1 waiting\n");
      Acquire(lock);
      CvarWait(cvar, lock);
      TracePrintf(1, "init: child 1 was signalled\n");
      
      CvarSignal(cvar);
      TracePrintf(1, "init: child 1 signalled\n");
    }
  }
  TracePrintf(1, "init: done\n");
  return rc;
} 
